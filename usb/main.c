#include <msp430.h> 
#include <descriptors.h>
#include <USB_API/USB_Common/device.h>
#include <USB_API/USB_Common/types.h>
#include <USB_API/USB_Common/usb.h> //USB specific function
#include <core/HAL_UCS.h>
#include <core/HAL_PMM.h>

#include <USB_API/USB_HID_API/UsbHid.h>
#include <usb_keymap.h> //HID key code
#include <msp430f6659.h>

#define TEST_USB_HID

#ifdef TEST_USB_HID
#include "timer_a.h"
int iCnt = 0;

#define COMPARE_VALUE 50000
#endif

/*
 * main.c
 */

VOID Init_Ports(VOID);
VOID Init_Clock(VOID);

BYTE report[8];                                 //Standard keyboard input report
BYTE buttonPressed = FALSE;

/*
 * ======== main ========
 */
VOID main(VOID) {
	BYTE i;

	WDTCTL = WDTPW + WDTHOLD;                              //Stop watchdog timer

	Init_Ports();   //Init ports (do first ports because clocks do change ports)
	SetVCore(3);
	Init_Clock();                                               //Init clocks
#ifdef TEST_USB_HID
	//Start timer in continuous mode sourced by SMCLK
	TIMER_A_configureContinuousMode(TIMER_A1_BASE, TIMER_A_CLOCKSOURCE_SMCLK,
			TIMER_A_CLOCKSOURCE_DIVIDER_1, TIMER_A_TAIE_INTERRUPT_DISABLE,
			TIMER_A_DO_CLEAR);

	//Initiaze compare mode
	TIMER_A_clearCaptureCompareInterruptFlag(TIMER_A1_BASE,
			TIMER_A_CAPTURECOMPARE_REGISTER_0);
	TIMER_A_initCompare(TIMER_A1_BASE, TIMER_A_CAPTURECOMPARE_REGISTER_0,
			TIMER_A_CAPTURECOMPARE_INTERRUPT_ENABLE,
			TIMER_A_OUTPUTMODE_OUTBITVALUE, COMPARE_VALUE);

	TIMER_A_startCounter(TIMER_A1_BASE, TIMER_A_CONTINUOUS_MODE);

	//Enter LPM0, enable interrupts
	__bis_SR_register(LPM0_bits + GIE);
#endif
	USB_init();                                                 //Init USB

	//Configure pushbuttons
	//S1 will represent a key getting pressed; S2 will represent the SHIFT key
	P1OUT |= (BIT6 | BIT7);     //Enable the integrated pullup for P1.6 and P1.7
	P1REN |= (BIT6 | BIT7);     //Enable the integrated pullup for P1.6 and P1.7
	P1IFG = 0;                                                 //Ensure no flags
	P1IE |= BIT6;                                  //Enable a pullup for S1 only

	USB_setEnabledEvents(
			kUSB_VbusOnEvent + kUSB_VbusOffEvent + kUSB_receiveCompletedEvent
					+ kUSB_UsbSuspendEvent + kUSB_UsbResumeEvent);

	//In case USB is already attached (meaning no VBUS event will
	//occur), manually start the connection
	if (USB_connectionInfo() & kUSB_vbusPresent) {
		USB_handleVbusOnEvent();
	}

	__enable_interrupt();                           //Enable interrupts globally
	while (1) {
		switch (USB_connectionState()) {
		case ST_USB_DISCONNECTED:
			__bis_SR_register(LPM3_bits + GIE);
			//Enter LPM3 w/interrupt.  Nothing for us to do while disconnected.
			_NOP();
			break;

		case ST_USB_CONNECTED_NO_ENUM:
			break;

		case ST_ENUM_ACTIVE:
			__bis_SR_register(LPM0_bits + GIE);
			//Enter LPM0 w/interrupt, until a keypress occurs

			if (buttonPressed) {               //Has a keypress really occurred?
											   //Then send the report created in the PORT1 ISR
				USBHID_sendReport((void *) &report, HID0_INTFNUM);
				//Send a blank report, to signal the end of the text
				for (i = 0; i < 8; i++) {
					report[i] = 0x00;
				}
				USBHID_sendReport((void *) &report, HID0_INTFNUM);

				buttonPressed = FALSE;						//Clear the flag
			}
			break;

		case ST_ENUM_SUSPENDED:
			__bis_SR_register(LPM3_bits + GIE);
			//Enter LPM3 w/interrupt.  Nothing for us to do while
			break;  //suspended.  (Remote wakeup isn't enabled in this example.)

		case ST_ENUM_IN_PROGRESS:
			break;

		case ST_NOENUM_SUSPENDED:
			__bis_SR_register(LPM3_bits + GIE);
			break;

		case ST_ERROR:
			_NOP();
			break;

		default:
			;
		}
	}  //while(1)
} //main()

/*
 * ======== Init_Clock ========
 */
VOID Init_Clock(VOID) {
	//Initialization of clock module
	if (USB_PLL_XT == 2) {
#if defined (__MSP430F552x) || defined (__MSP430F550x)
		P5SEL |= 0x0C;                               //enable XT2 pins for F5529
#elif defined (__MSP430F563x_F663x) || defined (__MSP430F565x_F665x)
		P7SEL |= 0x0C;
#endif

		//use REFO for FLL and ACLK
		UCSCTL3 = (UCSCTL3 & ~(SELREF_7)) | (SELREF__REFOCLK);
		UCSCTL4 = (UCSCTL4 & ~(SELA_7)) | (SELA__REFOCLK);

		//MCLK will be driven by the FLL (not by XT2), referenced to the REFO
		Init_FLL_Settle(USB_MCLK_FREQ / 1000, USB_MCLK_FREQ / 32768); //Start the FLL, at the freq indicated by the config
																	  //constant USB_MCLK_FREQ
		XT2_Start(XT2DRIVE_0);                         //Start the "USB crystal"
	} else {
#if defined (__MSP430F552x) || defined (__MSP430F550x)
		P5SEL |= 0x10;                                      //enable XT1 pins
#endif
		//Use the REFO oscillator to source the FLL and ACLK
		UCSCTL3 = SELREF__REFOCLK;
		UCSCTL4 = (UCSCTL4 & ~(SELA_7)) | (SELA__REFOCLK);

		//MCLK will be driven by the FLL (not by XT2), referenced to the REFO
		Init_FLL_Settle(USB_MCLK_FREQ / 1000, USB_MCLK_FREQ / 32768); //set FLL (DCOCLK)

		XT1_Start(XT1DRIVE_0);                         //Start the "USB crystal"
	}
}

/*
 * ======== Init_Ports ========
 */VOID Init_Ports(VOID) {
	//Initialization of ports (all unused pins as outputs with low-level
	P1OUT = 0x00;
	P1DIR = 0xFF;
	P2OUT = 0x00;
	P2DIR = 0xFF;
	P3OUT = 0x00;
	P3DIR = 0xFF;
	P4OUT = 0x00;
	P4DIR = 0xFF;
	P5OUT = 0x00;
	P5DIR = 0xFF;
	P6OUT = 0x00;
	P6DIR = 0xFF;
	P7OUT = 0x00;
	P7DIR = 0xFF;
	P8OUT = 0x00;
	P8DIR = 0xFF;
#if defined (__MSP430F563x_F663x)
	P9OUT = 0x00;
	P9DIR = 0xFF;
#endif
}

/*
 * ======== UNMI_ISR ========
 */
#pragma vector = UNMI_VECTOR
__interrupt VOID UNMI_ISR(VOID) {
	switch (__even_in_range(SYSUNIV, SYSUNIV_BUSIFG)) {
	case SYSUNIV_NONE:
		__no_operation();
		break;
	case SYSUNIV_NMIIFG:
		__no_operation();
		break;
	case SYSUNIV_OFIFG:
		UCSCTL7 &= ~(DCOFFG + XT1LFOFFG + XT2OFFG); //Clear OSC flaut Flags fault flags
		SFRIFG1 &= ~OFIFG;                          //Clear OFIFG fault flag
		break;
	case SYSUNIV_ACCVIFG:
		__no_operation();
		break;
	case SYSUNIV_BUSIFG:
		//If bus error occured - the cleaning of flag and re-initializing of USB is
		//required.
		SYSBERRIV = 0;                          //clear bus error flag
		USB_disable();                          //Disable
	}
}

#ifdef TEST_USB_HID
#pragma vector=TIMER1_A0_VECTOR
__interrupt void TIMER1_A0_ISR(void) {
	++iCnt;
	if (iCnt == 1000) {
		iCnt = 0;
		report[0] = 0x00;
		report[1] = 0x00;                       //Reserved
		report[2] = USB_HID_VK_T; //The remaining six bytes are for entered text.  It allows up
		report[3] = USB_HID_VK_E; //to six in a single report.  Since we don't have the hardware
		report[4] = USB_HID_VK_S; //for a true keyboard, let's just fill it with a six-letter word.
		report[5] = USB_HID_VK_T; //A real keyboard app would put whatever keys had been pressed
		report[6] = USB_HID_VK_ENTER;                  //since the last report.
		report[7] = 0x00;
		buttonPressed = 1;
		__bic_SR_register_on_exit(LPM3_bits);
	}

	//Add Offset to CCR0
	TIMER_A_setCompareValue(TIMER_A1_BASE, TIMER_A_CAPTURECOMPARE_REGISTER_0,
			COMPARE_VALUE);
}
#endif
