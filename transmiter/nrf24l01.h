/*
 * nrf24l01.h
 *
 *  Created on: Jul 2, 2013
 *      Author: Cong Danh
 */

#ifndef NRF24L01_H_
#define NRF24L01_H_

#include <stdint.h>
/**
 * Indicate the MCU is woke up by nRF24L01+ RX or not.
 *
 * @value	1	if MCU is woke up by nRF24L01+ RX.
 * 			0 otherwise.
 */
extern uint8_t nrf24l01_is_usb_post_back;
/* START: Initial Function */

/**
 * Initialize nRF24L01+ module.
 */
void	nrf24l01_init();

/* END: Initial Function */

/* START: IO Function */

/**
 * Send data via nRF24L01+.
 *
 * @param data data to send.
 * @param length length of sent data.
 * @return value of STATUS register of nRF24L01+.
 */
uint8_t	nrf24l01_write(uint8_t *data, int length);
/**
 * Read data arrived at nRF24L01+.
 *
 * @param buffer read buffer.
 * @param len	 number of byte to read.
 * @return	value of STATUS register of nRF24L01+.
 */
uint8_t	nrf24l01_read(uint8_t *buffer, int len);

/* END: IO Function */

/**
 * Check if RX buffer is empty or not.
 *
 * @return	1 if RX buffer is empty.
 * 			0 if RX buffer is not empty.
 */
uint8_t nrf24l01_rx_buffer_not_empty(void);

#endif /* NRF24L01_H_ */
