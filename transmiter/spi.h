/*
 * spi.h
 *
 *  Created on: Jun 8, 2013
 *      Author: CongDanh
 */

#ifndef SPI_H_
#define SPI_H_

/**
 * Initialize SPI Interface.
 **/
extern void spi_init(void);
/**
 * Exchange data via SPI.
 *
 * @param c	sent data.
 * @return	received data.
 */
extern unsigned char spi_xferchar(unsigned char c);

#endif /* SPI_H_ */
