/*
 * keyboard.h
 *
 *  Created on: Jun 12, 2013
 *      Author: CongDanh
 */

#ifndef KEYBOARD_H_
#define KEYBOARD_H_
#include "stdint.h"
#include "hardware.h"

//#define SPI_KEYBOARD_TEST 18

/*! 8-bit register definition */
typedef uint8_t register_t;

typedef uint8_t key_t;

typedef union {
	struct {
		uint32_t header;
		uint16_t crc;
		uint8_t updated;
		register_t empty_slots;
		union {
			uint16_t data[4];
			key_t keys[8];
		} keys;
	} report;
	uint8_t data[16];
} report_t;

extern report_t report;

/**
 * Initialization of ports.
 */
extern void keyboard_init();
/**
 * Polling keyboard and send pressed key to RF.
 */
extern void keyboard_poll();
/**
 * Notify User that keyboard is ready.
 */
inline void keyboard_notify_ready(void) {
	_disable_interrupts();
	// Turn on all LED.
	CAPS_OUT |= CAPS_PIN;
	NUM_LK_OUT |= NUM_LK_PIN;
	SCROLL_LK_OUT |= SCROLL_LK_PIN;
	__delay_cycles(200000);
	// Turn off all LED.
	CAPS_OUT &= ~CAPS_PIN;
	NUM_LK_OUT &= ~NUM_LK_PIN;
	SCROLL_LK_OUT &= ~SCROLL_LK_PIN;
	__delay_cycles(200000);
	// Turn on all LED.
	CAPS_OUT |= CAPS_PIN;
	NUM_LK_OUT |= NUM_LK_PIN;
	SCROLL_LK_OUT |= SCROLL_LK_PIN;
	__delay_cycles(200000);
	// Turn off all LED.
	CAPS_OUT &= ~CAPS_PIN;
	NUM_LK_OUT &= ~NUM_LK_PIN;
	SCROLL_LK_OUT &= ~SCROLL_LK_PIN;
	_enable_interrupts();
}

#endif /* KEYBOARD_H_ */
