/*
 * timer.c
 *
 *  Created on: 25-07-2013
 *      Author: CongDanh
 */
#include "timer.h"
#include <msp430.h>

/******************************************************************************
*
 * @brief   Tick Timer interrupt service routine
 *  Wakes-up the mcu, checks if there was an overflow (previous loop didn't finish
 *  in the specified interval
 *
 * @return  NONE
 *****************************************************************************/
#pragma vector=TIMER0_A0_VECTOR
__interrupt void timer0_irq(void)
{
    TA0CCTL1 ^= CCIS0;                   // Create SW capture of CCR1
    //count = TA0CCR1;                    // Save result
    TA0CCTL1 ^= CCIS0;                   // Re-enable capture on CCR1


    LPM0_EXIT;     // exit LPM
    __no_operation();

}
