/*
 * hardware.h
 *
 *  Created on: Jul 15, 2013
 *      Author: DuyHung
 */

#ifndef HARDWARE_H_
#define HARDWARE_H_

//
//  Header files
//
#include "msp430.h"

//
//  Configuration definitions
//
#ifndef USB_MCLK_FREQ
#define MCLK_FREQ   8000000
#else
#define MCLK_FREQ   USB_MCLK_FREQ
#endif


// Caps Lock
#define CAPS_DIR			P8DIR
#define CAPS_OUT			P8OUT
#define	CAPS_PIN			BIT1
// Num Lock
#define	NUM_LK_DIR			P8DIR
#define NUM_LK_OUT			P8OUT
#define NUM_LK_PIN			BIT0
// Scroll Lock
#define	SCROLL_LK_DIR		P8DIR
#define SCROLL_LK_OUT		P8OUT
#define SCROLL_LK_PIN		BIT2

//
//  Constant HW definitions
//
/*! Port4 KSI[0:7] */
#define KSI_PORTR    (P4IN)       /*! KSI Read Port */
#define KSI_PORTW    (P4OUT)      /*! KSI Write Port */
#define KSI_PORTDIR  (P4DIR)      /*! KSI Dir */
#define KSI_IES      (P4IES)      /*! Interrupt Edge select */
#define KSI_IFG      (P4IFG)      /*! Interrupt Flag */
#define KSI_IE       (P4IE)       /*! Interrupt Enable */
#define KSI0        BIT0
#define KSI1        BIT1
#define KSI2        BIT2
#define KSI3        BIT3
#define KSI4        BIT4
#define KSI5        BIT5
#define KSI6        BIT6
#define KSI7        BIT7
#define KSI_ALL        (BIT0|BIT1|BIT2|BIT3|BIT4|BIT5|BIT6|BIT7)
#define KSI_VECTOR      PORT4_VECTOR

/*! Port4 KS0[2:9] */
#define KSO2_PORT     ((uint16_t) &P1OUT)    /*! KSO2 Port */
#define KSO2        BIT0        /*! KSO2 Pin */
#define KSO3_PORT     ((uint16_t) &P1OUT)    /*! KSO3 Port */
#define KSO3        BIT1        /*! KSO3 Pin */
#define KSO4_PORT     ((uint16_t) &P1OUT)    /*! KSO4 Port */
#define KSO4        BIT2        /*! KSO4 Pin */
#define KSO5_PORT     ((uint16_t) &P1OUT)    /*! KSO5 Port */
#define KSO5        BIT3        /*! KSO5 Pin */
#define KSO6_PORT     ((uint16_t) &P1OUT)    /*! KSO6 Port */
#define KSO6        BIT4        /*! KSO6 Pin */
#define KSO7_PORT     ((uint16_t) &P1OUT)    /*! KSO7 Port */
#define KSO7        BIT5        /*! KSO7 Pin */
#define KSO8_PORT     ((uint16_t) &P1OUT)    /*! KSO7 Port */
#define KSO8        BIT6        /*! KSO7 Pin */
#define KSO9_PORT     ((uint16_t) &P1OUT)    /*! KSO7 Port */
#define KSO9        BIT7        /*! KSO7 Pin */
#define KSO_PORT1_ALL      (BIT0|BIT1|BIT2|BIT3|BIT4|BIT5|BIT6|BIT7)

/*! Port3 KS0[8:12] */
#define KSO10_PORT     ((uint16_t) &P3OUT)    /*! KSO8 Port */
#define KSO10        BIT0        /*! KSO8 Pin */
#define KSO11_PORT     ((uint16_t) &P3OUT)    /*! KSO9 Port */
#define KSO11        BIT1        /*! KSO9 Pin */
#define KSO12_PORT    ((uint16_t) &P3OUT)   /*! KSO10 Port */
#define KSO12       BIT2        /*! KSO10 Pin */
#define KSO13_PORT    ((uint16_t) &P3OUT)   /*! KSO11 Port */
#define KSO13       BIT3        /*! KSO11 Pin */
#define KSO14_PORT    ((uint16_t) &P3OUT)   /*! KSO12 Port */
#define KSO14       BIT4        /*! KSO12 Pin */
#define KSO15_PORT    ((uint16_t) &P3OUT)   /*! KSO12 Port */
#define KSO15       BIT5        /*! KSO12 Pin */
#define KSO16_PORT    ((uint16_t) &P3OUT)   /*! KSO12 Port */
#define KSO16       BIT6        /*! KSO12 Pin */
#define KSO17_PORT    ((uint16_t) &P3OUT)   /*! KSO12 Port */
#define KSO17       BIT7        /*! KSO12 Pin */
#define KSO_PORT3_ALL   (BIT0|BIT1|BIT2|BIT3|BIT4|BIT5|BIT6|BIT7)

/*! Port4 */
#define LED_PORT_W  P8OUT           /*! LED Output Port */
#define LEDNUM      BIT1            /*! LED1 pin */
#define LEDCAPS     BIT2            /*! LED2 pin */
#define LED3     	BIT3            /*! LED3 pin */

/*! Port5 */
#define KSO0_PORT     ((uint16_t) &P5OUT)    /*! KSO0 Port */
#define KSO0        BIT4        /*! KSO0 Pin */
#define KSO1_PORT     ((uint16_t) &P5OUT)    /*! KSO1 Port */
#define KSO1        BIT5        /*! KSO1 Pin */
#define KSO_PORT5_ALL   (KSO0|KSO1)

#endif /* HARDWARE_H_ */
