/*
 * timer.h
 *
 *  Created on: 25-07-2013
 *      Author: CongDanh
 */

#ifndef TIMER_H_
#define TIMER_H_
#include <msp430.h>

inline void timer_init(void){
    // Configure Timer Trigger TA0.0
    // TA0.0 Period (40000/100= ~400MHz = 2.5ms)
    TA0CCR0 = 100;

    TA0CCTL0 = CCIE;    // Enable Timer interrupt
    TA0CTL = TASSEL__ACLK | MC__STOP | TACLR;   // ACLK, stopped
}

inline void timer_start(void) {
	TA0CTL |= MC__UP;
}

inline void timer_stop(void) {
	TA0CTL &= ~MC_3;
}

#endif /* TIMER_H_ */
