/*
 * usb_keymap.h
 *
 *			Created on: Jun 12, 2013
 *			Author: CongDanh
 */

#ifndef USB_KEYMAP_H_
#define USB_KEYMAP_H_

#define USB_HID_REVERSED			0x00
#define USB_HID_ERROR_ROLL_OVER		0x01
#define USB_POST_FAIL				0x02
#define USB_ERR_UNDEFINED			0x03
#define USB_HID_VK_A				0x04
#define USB_HID_VK_B				0x05
#define USB_HID_VK_C				0x06
#define USB_HID_VK_D				0x07
#define USB_HID_VK_E				0x08
#define USB_HID_VK_F				0x09
#define USB_HID_VK_G				0x0A
#define USB_HID_VK_H				0x0B
#define USB_HID_VK_I				0x0C
#define USB_HID_VK_J				0x0D
#define USB_HID_VK_K				0x0E
#define USB_HID_VK_L				0x0F
#define USB_HID_VK_M				0x10
#define USB_HID_VK_N				0x11
#define USB_HID_VK_O				0x12
#define USB_HID_VK_P				0x13
#define USB_HID_VK_Q				0x14
#define USB_HID_VK_R				0x15
#define USB_HID_VK_S				0x16
#define USB_HID_VK_T				0x17
#define USB_HID_VK_U				0x18
#define USB_HID_VK_V				0x19
#define USB_HID_VK_W				0x1A
#define USB_HID_VK_X				0x1B
#define USB_HID_VK_Y				0x1C
#define USB_HID_VK_Z				0x1D
#define USB_HID_VK_1				0x1E /* Keyboard 1 and ! */
#define USB_HID_VK_EXCLAMATION		USB_HID_VK_1
#define USB_HID_VK_2				0x1F /* Keyboard 2 and @ */
#define USB_HID_VK_AT				USB_HID_VK_2
#define USB_HID_VK_3				0x20 /* Keyboard 3 and # */
#define USB_HID_VK_HASH				USB_HID_VK_3
#define USB_HID_VK_4				0x21 /* Keyboard 4 and $ */
#define USB_HID_VK_DOLLAR			USB_HID_VK_4
#define USB_HID_VK_5				0x22 /* Keyboard 5 and % */
#define USB_HID_VK_PERCENT			USB_HID_VK_5
#define USB_HID_VK_6				0x23 /* Keyboard 6 and ^ */
#define USB_HID_VK_XOR				USB_HID_VK_6
#define USB_HID_VK_7				0x24 /* Keyboard 7 and & */
#define USB_HID_VK_AMPERSANT		USB_HID_VK_7
#define USB_HID_VK_8				0x25 /* Keyboard 8 and * */
#define USB_HID_VK_ASTERISK			USB_HID_VK_8
#define USB_HID_VK_9				0x26 /* Keyboard 9 and ( */
#define USB_HID_VK_0				0x27 /* Keyboard 0 and ) */
#define USB_HID_VK_ENTER			0x28
#define USB_HID_VK_ESC				0x29
#define USB_HID_VK_BACKSPACE		0x2A /* Keyboard <----- */
#define USB_HID_VK_TAB				0x2B
#define USB_HID_VK_SPACEBAR			0x2C
#define USB_HID_VK_MINUS			0x2D /* Keyboard - and _ */
#define USB_HID_VK_UNDERSCORE		USB_HID_VK_MINUS
#define USB_HID_VK_EQUAL_PLUS		0x2E /* Keyboard = and + */
#define USB_HID_VK_LEFT_BRACKET		0x2F /* Keyboard [ and { */
#define USB_HID_VK_RIGHT_BRACKET	0x30 /* Keyboard ] and } */
#define USB_HID_VK_BACKSPLASH		0x31 /* Keyboard \ and | */
#define USB_HID_VK_VERTICAL_BAR		USB_HID_VK_BACKSPLASH
#define USB_HID_NON_US_VK_HASH		0x32
#define USB_HID_VK_SEMICOLON		0x33 /* Keyboard ; and : */
#define USB_HID_VK_APOSTROPHE		0x34 /* Keyboard ' and " */
#define USB_HID_VK_TILDE			0x35 /* ~ and ` */
#define USB_HID_VK_GRAVE_ANCENT		USB_HID_VK_TILDE
#define USB_HID_VK_COMMA			0x36 /* , and < */
#define USB_HID_VK_SMALL			USB_HID_VK_COMMA
#define USB_HID_VK_PERIOD			0x37 /* . and > */
#define USB_HID_VK_BIG				USB_HID_VK_PERIOD
#define USB_HID_VK_SLASH			0x38 /* / and ? */
#define USB_HID_VK_QUESTION			USB_HID_VK_SLASH
#define USB_HID_VK_CAPSLOCK			0x39
#define USB_HID_VK_F1				0x3A
#define USB_HID_VK_F2				0x3B
#define USB_HID_VK_F3				0x3C
#define USB_HID_VK_F4				0x3D
#define USB_HID_VK_F5				0x3E
#define USB_HID_VK_F6				0x3F
#define USB_HID_VK_F7				0x40
#define USB_HID_VK_F8				0x41
#define USB_HID_VK_F9				0x42
#define USB_HID_VK_F10				0x43
#define USB_HID_VK_F11				0x44
#define USB_HID_VK_F12				0x45
#define USB_HID_VK_PRINT_SCREEN		0x46
#define USB_HID_VK_SCROLLLOCK		0x47
#define USB_HID_VK_PAUSE			0x48
#define USB_HID_VK_INSERT			0x49
#define USB_HID_VK_HOME				0x4A
#define USB_HID_VK_PAGE_UP			0x4B
#define USB_HID_VK_DELETE			0x4C
#define USB_HID_VK_END				0x4D
#define USB_HID_VK_PAGE_DOWN		0x4E
#define USB_HID_VK_RIGHT_ARROW		0x4F
#define USB_HID_VK_LEFT_ARROW		0x50
#define USB_HID_VK_DOWN_ARROW		0x51
#define USB_HID_VK_UP_ARROW			0x52
#define USB_HID_KEYPAD_NUMLCK		0x53 /* Keypad Numlock. */
#define USB_HID_KEYPAD_SLASH		0x54 /* Keypad / */
#define USB_HID_KEYPAD_ASTERISK		0x55 /* Keypad * */
#define USB_HID_KEYPAD_MINUS		0x56 /* Keypad - */
#define USB_HID_KEYPAD_PLUS			0x57 /* Keypad + */
#define USB_HID_KEYPAD_ENTER		0x58
#define USB_HID_KEYPAD_1			0x59 /* Keypad 1 and End */
#define USB_HID_KEYPAD_END			USB_HID_KEYPAD_1
#define USB_HID_KEYPAD_2			0x5A /* Keypad 2 and Down Arrow */
#define USB_HID_KEYPAD_DOWN			USB_HID_KEYPAD_2
#define USB_HID_KEYPAD_3			0x5B /* Keypad 3 and Page Down */
#define USB_HID_KEYPAD_PAGE_DN		USB_HID_KEYPAD_3
#define USB_HID_KEYPAD_4			0x5C /* Keypad 4 and Left Arrow */
#define USB_HID_KEYPAD_LEFT			USB_HID_KEYPAD_4
#define USB_HID_KEYPAD_5			0x5D
#define USB_HID_KEYPAD_6			0x5E /* Keypad 6 and Right Arrow */
#define USB_HID_KEYPAD_RIGHT		USB_HID_KEYPAD_6
#define USB_HID_KEYPAD_7			0x5F /* Keypad 7 and Home */
#define USB_HID_KEYPAD_HOME			USB_HID_KEYPAD_7
#define USB_HID_KEYPAD_8			0x60 /* Keypad 8 and Up Arrow */
#define USB_HID_KEYPAD_UP			USB_HID_KEYPAD_8
#define USB_HID_KEYPAD_9			0x61 /* Keypad 9 and Page Up */
#define USB_HID_KEYPAD_PAGE_UP		USB_HID_KEYPAD_9
#define USB_HID_KEYPAD_0			0x62 /* Keypad 0 and Insert */
#define USB_HID_KEYPAD_INS			USB_HID_KEYPAD_0
#define USB_HID_KEYPAD_PERIOD		0x63 /* Keypad . and Delete */
#define USB_HID_KEYPAD_DEL			USB_HID_KEYPAD_PERIOD

/* 0x64 for Non US \ and |, not implement here. */

#define USB_HID_VK_APPLICATION			0x65 /* Used in Windows and *nix only. */
#define USB_HID_VK_Power			0x66 /* Used in Mac and *nix only. */

/* From 0x67 to 0x6A used in Mac only. */


#define USB_HID_VK_LEFT_CTRL		0xE0
#define USB_HID_VK_LEFT_SHIFT		0xE1
#define USB_HID_VK_LEFT_ALT			0xE2
#define USB_HID_VK_LEFT_GUI			0xE3
#define USB_HID_VK_RIGHT_CTRL		0xE4
#define USB_HID_VK_RIGHT_SHIFT		0xE5
#define USB_HID_VK_RIGHT_ALT		0xE6
#define USB_HID_VK_RIGHT_GUI		0xE7


#endif /* USB_KEYMAP_H_ */
