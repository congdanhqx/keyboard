/*
 * main.c
 */
#include <msp430.h> 

#include <stdint.h>

#include "keyboard.h"
#include "nrf24l01.h"
#include "aes.h"
#include "usb_hid_output.h"
#include "hardware.h"
#include <stdlib.h>
#include <time.h>

/**
 * Update CRC for report.
 */
inline void crc_update();
/**
 * Check CRC of report is valid or not.
 *
 * @param report	pointer to report to check.
 *
 * @return	true if CRC is valid.
 * 			false otherwise.
 */
uint8_t crc_is_valid(report_t *report);

/**
 * Check CRC of CRC Response is valid or not.
 *
 * @param report	pointer to report to check.
 *
 * @return	true if CRC is valid.
 * 			false otherwise.
 */
uint8_t crc_is_valid2(report_t *report);

/**
 * Generate random value for report.
 */
inline void report_random(void);

/**
 * Reset report to its initial value.
 */
inline void report_reset(void);

/**
 * Request CRC initial key from USB module.
 */
inline void request_crc_key(void);

/**
 * Buffer for send/receive from nRF24L01+.
 */
uint8_t nrf24l01_buffer[16];

/**
 * Key used for Initial CRC algorithm.
 */
uint16_t crc_seed_key;

int main(void) {
#if 0
	uint8_t t[16] = {0x45, 0x34, 0x46, 0x47, 0x48, 0x49, 0x50, 0x51, 0x52, 0x53, 0x54, 0x55, 0x56, 0x57, 0x58, 0x59};
#endif
	WDTCTL = WDTPW | WDTHOLD;	// Stop watchdog timer

	// Initialize all module.
	keyboard_init();
	nrf24l01_init();
	_enable_interrupts();
	// Request CRC seed key from USB module.
	request_crc_key();
	// Notify user that keyboard is turned on.
	// keyboard_notify_ready();

	for (;;) {
		_bis_SR_register(LPM3_bits + GIE);
		if (nrf24l01_is_usb_post_back) {
			report_t postback;
			// Reset this value to prevent unpredictable loop in the future.
			nrf24l01_is_usb_post_back = 0;
			// Read data sent from USB Module and decrypt it.
			nrf24l01_read(nrf24l01_buffer, 16);
			aes_decrypt(nrf24l01_buffer, postback.data);
			// Check if received data is fake or not.
			if (postback.report.header == 0xfa69dead) {
				if (postback.report.updated == 0x01) {
					if (crc_is_valid(&postback)) {
						uint8_t hid_output = postback.report.keys.keys[0];
						// Parse the data and turn on, off the corresponding LED.
						if (hid_output & USB_HID_OUT_NUM_LCK) {
							NUM_LK_OUT |= NUM_LK_PIN;
						} else {
							NUM_LK_OUT &= ~NUM_LK_PIN;
						}
						if (hid_output & USB_HID_OUT_CAP_LCK) {
							CAPS_OUT |= CAPS_PIN;
						} else {
							CAPS_OUT &= ~CAPS_PIN;
						}
						if (hid_output & USB_HID_OUT_SCR_LCK) {
							SCROLL_LK_OUT |= SCROLL_LK_PIN;
						} else {
							SCROLL_LK_OUT &= ~SCROLL_LK_PIN;
						}
					}
				} else if (postback.report.updated == 0xFF) {
					if (crc_is_valid2(&postback)) {
						crc_seed_key = postback.report.keys.data[0];
						keyboard_notify_ready();
						_nop();
					}
				}
			}
		}
		// Scan key matrix.
		keyboard_poll();
		// If key pressed, encrypt the report and send the encrypted report the USB Module.
		if (report.report.updated) {
			crc_update();
			aes_encrypt(report.data, nrf24l01_buffer);
			nrf24l01_write(nrf24l01_buffer, 16);
			// Reset the updated value to prevent send duplicate report.
			report.report.updated = 0;
		}
	}
}

/**
 * Update CRC for report.
 */
inline void crc_update(void) {
// Reset the CRC initialization and result register.
	CRCINIRES = crc_seed_key;
// Process input data.
	CRCDI = report.report.keys.data[0];
	CRCDI = report.report.keys.data[1];
	CRCDI = report.report.keys.data[2];
	CRCDI = report.report.keys.data[3];
// Get Result.
	report.report.crc = CRCINIRES;
}

/**
 * Check CRC of report is valid or not.
 *
 * @param report	pointer to report to check.
 *
 * @return	true if CRC is valid.
 * 			false otherwise.
 */
uint8_t crc_is_valid(report_t *report) {
// Reset the CRC initialization and result register.
	CRCINIRES = crc_seed_key;
// Process input data.
	CRCDI = report->report.keys.data[0];
	CRCDI = report->report.keys.data[1];
	CRCDI = report->report.keys.data[2];
	CRCDI = report->report.keys.data[3];
// Get Result.
	return CRCINIRES == report->report.crc;
}

/**
 * Check CRC of CRC Response is valid or not.
 *
 * @param report	pointer to report to check.
 *
 * @return	true if CRC is valid.
 * 			false otherwise.
 */
uint8_t crc_is_valid2(report_t *report) {
// Reset the CRC initialization and result register.
	CRCINIRES = 0x69FAu;
// Process input data.
	CRCDI = report->report.keys.data[0];
	CRCDI = report->report.keys.data[1];
	CRCDI = report->report.keys.data[2];
	CRCDI = report->report.keys.data[3];
// Get Result.
	return CRCINIRES == report->report.crc;
}

/**
 * Generate random value for report.
 */
inline void report_random(void) {
	srand(time(NULL));
// Reset the CRC initialization and result register.
	CRCINIRES = 0x69FAu;
	CRCDI = report.report.keys.data[0] = rand();
	CRCDI = report.report.keys.data[1] = rand();
	CRCDI = report.report.keys.data[2] = rand();
	CRCDI = report.report.keys.data[3] = rand();
	report.report.crc = CRCINIRES;
}

/**
 * Reset report to its initial value.
 */
inline void report_reset(void) {
	report.report.updated = 0;
	report.report.keys.data[0] = 0;
	report.report.keys.data[1] = 0;
	report.report.keys.data[2] = 0;
	report.report.keys.data[3] = 0;
}

inline void request_crc_key(void) {
	report.report.updated = 0xFF;
	report_random();
	aes_encrypt(report.data, nrf24l01_buffer);
	report_reset();
	nrf24l01_write(nrf24l01_buffer, 16);
}

/**
 * Trap all unused interrupt.
 *
 * Trap all unused interrupt to prevent  jumping to unknown addresses,
 * execution of erroneous code which at the very least could lead to
 * application crashing, unexpected application behavior, or more
 * severely memory erasure or electrical damage.
 * TI compiler's Warning #10374.
 */
#pragma vector=ADC12_VECTOR
#pragma vector=COMP_B_VECTOR
#pragma vector=DAC12_VECTOR
#pragma vector=DMA_VECTOR
#pragma vector=LCD_B_VECTOR
#pragma vector=PORT1_VECTOR
#pragma vector=PORT3_VECTOR
#pragma vector=RTC_VECTOR
#pragma vector=SYSNMI_VECTOR
#pragma vector=TIMER0_A1_VECTOR
#pragma vector=TIMER0_B0_VECTOR
#pragma vector=TIMER0_B1_VECTOR
#pragma vector=TIMER1_A1_VECTOR
#pragma vector=TIMER2_A0_VECTOR
#pragma vector=TIMER2_A1_VECTOR
#pragma vector=UNMI_VECTOR
#pragma vector=USB_UBM_VECTOR
#pragma vector=USCI_A0_VECTOR
#pragma vector=USCI_A1_VECTOR
#pragma vector=USCI_A2_VECTOR
#pragma vector=USCI_B1_VECTOR
#pragma vector=USCI_B2_VECTOR
#pragma vector=WDT_VECTOR
__interrupt void unused_irq(void) {
// Uncomment this line for debugging purpose
// while (1);
// Uncomment this line for reset purpose
// PMMCTL0 = PMMPW | PMMSWBOR;         // Apply PMM password and trigger SW BOR
}
