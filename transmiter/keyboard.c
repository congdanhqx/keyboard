/*
 * keyboard.c
 *
 *  Created on: Jun 12, 2013
 *      Author: CongDanh
 */

#include "keyboard.h"

#include <msp430.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>

#include "timer.h"
#include "usb_keymap.h"


#define KSO_PINS            18      // Number of KSO (row) pins
#define KSI_PINS            8       // Number of KSI (column) pins
#define DELAY_DKS_KSO       5       // Delay after setting KSO output
#define DEBOUNCE_CYCLES     1       //  Debounce cycles (in tick counts)
#define INACTIVITY_TIMEOUT  8       // timeout before going to interrupt mode (in tick counts)
/*! Set all KSO pins as output high to detect interrupt on KSI pins */
#define SET_KSO_INTERRUPT \
	P1OUT = 0xFF; \
	P1DIR = 0xFF; \
	\
	P3OUT = 0xFF; \
	P3DIR = 0xFF; \
	\
	P5OUT |= KSO_PORT5_ALL; \
	P5DIR |= KSO_PORT5_ALL;

/*! Set KSO pins as input with pull-down to poll each row */
#define SET_KSO_POLL \
	P1OUT = 0; \
	P1DIR = 0; \
	P1REN = 0xFF;\
	\
	P3OUT = 0; \
	P3DIR = 0; \
	P3REN = 0xFF;\
	\
	P5OUT &= ~KSO_PORT5_ALL; \
	P5DIR &= ~KSO_PORT5_ALL; \
	P5REN |= KSO_PORT5_ALL;

typedef enum {
	SCAN_MODE_POLLING, SCAN_MODE_INTERRUPT
} scan_mode_t;

/*! Port+pin definition */
typedef struct {
	/*!< Port pointer */
	uint16_t portw;
	/*!< Port mask */
	uint8_t port_mask;
} port_t;

report_t report = {{
		0xfa69dead, /* Header */
		0x00, 0x00, 0xFF, /* CRC, updated, empty_slots */
		0x00, 0x00, 0x00, 0x00, /* report data. */
}};
uint8_t kb_counter_key[KSO_PINS * KSI_PINS];
uint8_t kb_col_pressed_by_row[KSO_PINS];
uint8_t kb_col_pressed_by_row_prev[KSO_PINS];
scan_mode_t kb_scan_mode;
uint16_t kb_inactive_counter;

/*! KSO hardware mapping. Provides a Hardware Abstraction layer allowing
 *  for easier portability. Simply change the port and pin according to your
 *  hardware
 */
// @formatter:off
const port_t kb_port_map[KSO_PINS] =
	{
			{ KSO0_PORT, KSO0 },
			{ KSO1_PORT, KSO1 },
			{ KSO2_PORT, KSO2 },
			{ KSO3_PORT, KSO3 },
			{ KSO4_PORT, KSO4 },
			{ KSO5_PORT, KSO5 },
			{ KSO6_PORT, KSO6 },
			{ KSO7_PORT, KSO7 },
			{ KSO8_PORT, KSO8 },
			{ KSO9_PORT, KSO9 },
			{ KSO10_PORT, KSO10 },
			{ KSO11_PORT, KSO11 },
			{ KSO12_PORT, KSO12 },
			{ KSO13_PORT, KSO13 },
			{ KSO14_PORT, KSO14 },
			{ KSO15_PORT, KSO15 },
			{ KSO16_PORT, KSO16 },
			{ KSO17_PORT, KSO17 }
	};
// @formatter:on

/*! Scancodes constant table
 *  Located in a reserved memory location
 *  Can be optimized according to different key maps
 **/
#pragma DATA_SECTION(kb_keymap, ".keymap")
#pragma DATA_ALIGN(kb_keymap, 256)
const uint8_t kb_keymap[144] = {
		USB_HID_REVERSED,	USB_HID_REVERSED,	USB_HID_REVERSED,	USB_HID_REVERSED,	USB_HID_REVERSED,	USB_HID_VK_LEFT_GUI,	USB_HID_REVERSED,	USB_HID_REVERSED,
		USB_HID_REVERSED,	USB_HID_REVERSED,	USB_HID_REVERSED,	USB_HID_REVERSED,	USB_HID_REVERSED,	USB_HID_VK_RIGHT_GUI,	USB_HID_REVERSED,	USB_HID_REVERSED,
		USB_HID_REVERSED,	USB_HID_VK_RIGHT_CTRL,	USB_HID_REVERSED,	USB_HID_REVERSED,	USB_HID_REVERSED,	USB_HID_VK_LEFT_CTRL,	USB_HID_REVERSED,	USB_HID_REVERSED,
		USB_HID_REVERSED,	USB_HID_VK_RIGHT_SHIFT,	USB_HID_REVERSED,	USB_HID_REVERSED,	USB_HID_REVERSED,	USB_HID_REVERSED,	USB_HID_VK_RIGHT_SHIFT,	USB_HID_REVERSED,
		USB_HID_REVERSED,	USB_HID_VK_Z, 	USB_HID_VK_A, 	USB_HID_VK_Q, 	USB_HID_VK_1, 	USB_HID_VK_TILDE,	USB_HID_VK_TAB,	USB_HID_VK_ESC,
		USB_HID_REVERSED,	USB_HID_VK_X, 	USB_HID_VK_S, 	USB_HID_VK_W, 	USB_HID_VK_2, 	USB_HID_VK_F1, 	USB_HID_VK_CAPSLOCK,	USB_HID_REVERSED,
		USB_HID_VK_APPLICATION,	USB_HID_VK_C, 	USB_HID_VK_D, 	USB_HID_VK_E, 	USB_HID_VK_3, 	USB_HID_VK_F2, 	USB_HID_VK_F3, 	USB_HID_VK_F4,
		USB_HID_VK_B, 	USB_HID_VK_V, 	USB_HID_VK_F, 	USB_HID_VK_R, 	USB_HID_VK_4, 	USB_HID_VK_5, 	USB_HID_VK_T, 	USB_HID_VK_G,
		USB_HID_VK_SPACEBAR,	USB_HID_VK_ENTER,	USB_HID_VK_BACKSPLASH,	USB_HID_REVERSED,	USB_HID_VK_F10, 	USB_HID_VK_F9, 	USB_HID_VK_BACKSPACE,	USB_HID_VK_F5,
		USB_HID_VK_N, 	USB_HID_VK_M, 	USB_HID_VK_J, 	USB_HID_VK_U, 	USB_HID_VK_7, 	USB_HID_VK_6, 	USB_HID_VK_Y, 	USB_HID_VK_H,
		USB_HID_REVERSED,	USB_HID_VK_COMMA,	USB_HID_VK_K, 	USB_HID_VK_I, 	USB_HID_VK_8, 	USB_HID_VK_EQUAL_PLUS,	USB_HID_VK_RIGHT_BRACKET,	USB_HID_VK_F6,
		USB_HID_REVERSED,	USB_HID_VK_PERIOD,	USB_HID_VK_L, 	USB_HID_VK_O, 	USB_HID_VK_9, 	USB_HID_VK_F8, 	USB_HID_VK_F7, 	USB_HID_REVERSED,
		USB_HID_VK_SLASH,	USB_HID_VK_I, 	USB_HID_VK_SEMICOLON,	USB_HID_VK_P, 	USB_HID_VK_0, 	USB_HID_VK_MINUS,	USB_HID_VK_LEFT_BRACKET,	USB_HID_VK_APOSTROPHE,
		USB_HID_VK_DOWN_ARROW,	USB_HID_KEYPAD_NUMLCK,	USB_HID_KEYPAD_1,	USB_HID_KEYPAD_7,	USB_HID_VK_F11, 	USB_HID_VK_DELETE,	USB_HID_KEYPAD_4,	USB_HID_REVERSED,
		USB_HID_VK_RIGHT_ARROW,	USB_HID_KEYPAD_SLASH,	USB_HID_KEYPAD_2,	USB_HID_KEYPAD_8,	USB_HID_VK_F12, 	USB_HID_VK_INSERT,	USB_HID_KEYPAD_5,	USB_HID_KEYPAD_0,
		USB_HID_KEYPAD_MINUS,	USB_HID_KEYPAD_ASTERISK,	USB_HID_KEYPAD_3,	USB_HID_KEYPAD_9,	USB_HID_VK_PAGE_DOWN,	USB_HID_VK_PAGE_UP,	USB_HID_KEYPAD_6,	USB_HID_KEYPAD_DEL,
		USB_HID_VK_LEFT_ARROW,	USB_HID_VK_PAUSE,	USB_HID_KEYPAD_ENTER,	USB_HID_KEYPAD_PLUS,	USB_HID_VK_END,	USB_HID_VK_HOME,	USB_HID_REVERSED,	USB_HID_VK_UP_ARROW,
		USB_HID_VK_RIGHT_ALT,	USB_HID_REVERSED,	USB_HID_REVERSED,	USB_HID_VK_SCROLLLOCK,	USB_HID_VK_PRINT_SCREEN,	USB_HID_REVERSED,	USB_HID_REVERSED,	USB_HID_VK_LEFT_ALT,

};

// Forward private function.
inline void report_add_key(key_t key);
inline void report_remove_key(key_t key);
uint8_t isKeyMasked(uint8_t row, uint8_t col);

void keyboard_init() {
	// all unused pins as outputs with low-level
	PADIR = 0xFFFF;
	PAOUT = 0x00;

	P3OUT = 0x00;
	P3DIR = 0xFF;

	// Keyboard Input in Port 4
	P4OUT = 0x00;
	P4DIR = 0x00;

	PCDIR = 0xFFFF;
	PCOUT = 0x00;

	PDOUT = 0x00;
	PDDIR = 0xFFFF;

	PEOUT = 0x00;
	PEDIR = 0xFFFF;

	timer_init();

	/* Initial Digital key scanning. */
	kb_inactive_counter = 0;
	memset(kb_col_pressed_by_row, 0, KSO_PINS);
	memset(kb_col_pressed_by_row_prev, 0, KSO_PINS);
	memset(kb_counter_key, 0, KSI_PINS * KSO_PINS);

	SET_KSO_INTERRUPT

	if (!KSI_PORTR) {
		kb_scan_mode = SCAN_MODE_INTERRUPT;
		KSI_IES = 0;     // Set rising edge interrupt
		KSI_IFG = 0;      // clear flags
		KSI_IE = 0xFF;        // Enable KSI interrupt
		timer_stop();
	} else {
		SET_KSO_POLL
		kb_scan_mode = SCAN_MODE_POLLING;
		KSI_IE = 0;       // Disable interrupts
		timer_start();
		kb_inactive_counter = 0;
	}
}
/**
 * Polling keyboard and send pressed key to RF.
 */
void keyboard_poll() {
	uint8_t row, col;
	uint8_t * port_ptr;
	uint8_t * port_dir;
	uint8_t idx;
	uint8_t key_pressed = 0;
	// _disable_interrupts();
	if (kb_scan_mode == SCAN_MODE_POLLING) {
		// Read all rows
		for (row = KSO_PINS; row;) {
			 --row;
			// Set pointers for PxOUT and PxDIR registers
			port_ptr = (uint8_t *) kb_port_map[row].portw;
			port_dir = (uint8_t *) kb_port_map[row].portw + 2;
			// Set KSO pin as output High
			*port_dir |= kb_port_map[row].port_mask;
			*port_ptr |= kb_port_map[row].port_mask;
			// Wait a few cycles before reading KSI
			__delay_cycles(5);
			// Read the KSI pins (processed later to remove ghost keys)
			kb_col_pressed_by_row[row] = KSI_PORTR;
			if (kb_col_pressed_by_row[row])
				key_pressed = 1;   // Check if any key is pressed
			// Set KSO pin as input with pull-down to avoid shorts
			*port_ptr &= ~kb_port_map[row].port_mask;
			*port_dir &= ~kb_port_map[row].port_mask;
			// Set KSI pins as output low to bleed off the charge
			KSI_PORTDIR = 0xFF;
			KSI_PORTW = 0;
			// Return KSI to Input
			KSI_PORTDIR = 0;
		}

		// Process keys (row and col)
		for (row = KSO_PINS; row;) {
			 --row;
			// If a key is pressed in row, or was pressed before, check which column
			if (kb_col_pressed_by_row[row] || kb_col_pressed_by_row_prev[row]) {
				for (col = KSI_PINS; col;) {
					 --col;
					// Scan which column is pressed in current row
					// XXX: shift row to the left 3 bits == multiply with 8 but it's faster.
					idx = (row << 3) + col;
					if (kb_col_pressed_by_row[row] & (1 << col)) {
						// If col+row is pressed, check debounce and ghost key
						if (!isKeyMasked(row, col)) {
							// A key was pressed and is not masked
							if (kb_counter_key[idx] < DEBOUNCE_CYCLES) {
								// Just increment the debounce counter
								++kb_counter_key[idx];
							} else if (kb_counter_key[idx] == DEBOUNCE_CYCLES) {
								// Add key to the report after debounce delay
								uint8_t key = kb_keymap[idx];
								report_add_key(key);
								++kb_counter_key[idx];
								switch (key) {
									case USB_HID_VK_CAPSLOCK:
										CAPS_OUT ^= CAPS_PIN;
										break;
									case USB_HID_KEYPAD_NUMLCK:
										NUM_LK_OUT ^= NUM_LK_PIN;
										break;
									case USB_HID_VK_SCROLLLOCK:
										SCROLL_LK_OUT ^= SCROLL_LK_PIN;
										break;
									default:
										break;
								}
							}
						}
					} else if (kb_counter_key[idx]  == DEBOUNCE_CYCLES + 1) {
						// If current col+row is not pressed, remove from report
						// if it was pressed previously (or just restart counter)
						report_remove_key(kb_keymap[idx]);
						kb_counter_key[idx] = 0;
					}
				}
			}
			// Back-up column value
			kb_col_pressed_by_row_prev[row] = kb_col_pressed_by_row[row];
		}
	}
	// If any key is pressed, reset the inactive counter
	if (key_pressed) {
		kb_inactive_counter = 0;
	} else {
		// If no key pressed for some time, change to Interrupt mode
		if (++kb_inactive_counter > 9) {
			timer_stop();
			SET_KSO_INTERRUPT
			kb_scan_mode = SCAN_MODE_INTERRUPT;
			KSI_IFG &= ~KSI_ALL;   // clear flags
			KSI_IE |= KSI_ALL;
		}
	}
	//_enable_interrupts();
}

inline void report_add_key(key_t key) {
	report_remove_key(key);
	if ((key & 0xE0) == 0xE0) {
		report.report.keys.keys[0] |= (1 << (key & 0x0F));
		report.report.updated = 1;
	} else {
		uint8_t slot = (report.report.empty_slots & 4) ? 2 :
						(report.report.empty_slots & 8) ? 3 :
						(report.report.empty_slots & 0x10) ? 4 :
						(report.report.empty_slots & 0x20) ? 5 :
						(report.report.empty_slots & 0x40) ? 6 :
						(report.report.empty_slots & 0x80) ? 7 : 0;
		if (slot) {
			report.report.keys.keys[slot] = key;
			report.report.empty_slots &= (uint8_t) (~(1 << slot));
			report.report.updated = 1;
		}
	}
}
inline void report_remove_key(key_t key) {
	if ((key & 0xE0) == 0xE0) {
		report.report.keys.keys[0] &= ~(1 << (key & 0x0F));
		report.report.updated = 1;
	} else {
		uint8_t slot;
		for (slot = 2; slot < 8; ++slot) {
			if ((!(report.report.empty_slots & (1 << slot)))
					&& report.report.keys.keys[slot] == key) {
				report.report.empty_slots |= (1 << slot);
				report.report.keys.keys[slot] = 0;
				report.report.updated = 1;
				return;
			}
		}
	}
}

/******************************************************************************
 *
 * @brief   Checks for ghost keys
 *  checks if current column+row press is valid or it can be caused by potential
 *  ghost keys
 *
 *  @param row Row corresponding to key being checked
 *  @param col Column corresponding to key being checked
 *
 * @return  1 Key can be caused by a ghost key, 0 key is valid
 *****************************************************************************/
uint8_t isKeyMasked(uint8_t row, uint8_t col) {
	uint8_t i;

	for (i = KSO_PINS; i;) {
		// Check if there are other columns pressed
		if (--i != row) {
			uint8_t p = kb_col_pressed_by_row[row] & kb_col_pressed_by_row[i];
			if (p && p != (1 << col))
				// Report if there's a potential ghost key (current col and row
				// is already pressed) so that the key is ignored
				return 1;
		}
	}
	return 0;
}

/******************************************************************************
 *
 * @brief   KSI_Isr
 *  Wakes-up the mcu due to a key press and goes to polling mode
 *
 * @return  NONE
 *****************************************************************************/
#pragma vector=KSI_VECTOR
__interrupt void KSI_irq(void) {
// Set KSO pins to start polling mode
	SET_KSO_POLL
	kb_scan_mode = SCAN_MODE_POLLING;
	KSI_IE = 0;
	kb_inactive_counter = 0;
// Start the polling timer
	timer_start();

	LPM0_EXIT;     // exit LPM
	__no_operation();

}
