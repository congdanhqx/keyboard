/*
 * aes.h
 *
 *  Created on: 06-08-2013
 *      Author: CongDanh
 */

#ifndef AES_H_
#define AES_H_

#include <stdint.h>
/**
 * Encrypt 128 bit data by the AES-128 algorithm.
 *
 * @param input	16 bytes plain data.
 * @param output output buffer for encrypted data, must be allocated at least 16 bytes.
 */
void aes_encrypt(uint8_t* input, uint8_t* output);

/**
 * Decrypt 128 bit data by the AES-128 algorithm.
 *
 * @param input	16 bytes encrypted data.
 * @param output output buffer for plain text, must be allocated at least 16 bytes.
 */
void aes_decrypt(uint8_t* input, uint8_t* output);

#endif /* AES_H_ */
