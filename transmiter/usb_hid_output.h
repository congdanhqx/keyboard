/*
 * usb_hid_output.h
 *
 *  Created on: 09-08-2013
 *      Author: CongDanh
 */

#ifndef USB_HID_OUTPUT_H_
#define USB_HID_OUTPUT_H_


#define USB_HID_OUT_NUM_LCK 0x01
#define USB_HID_OUT_CAP_LCK 0x02
#define USB_HID_OUT_SCR_LCK 0x04
#define USB_HID_OUT_COMPOSE	0x08
#define USB_HID_OUT_KANA	0x10


#endif /* USB_HID_OUTPUT_H_ */
