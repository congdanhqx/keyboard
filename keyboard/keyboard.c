/*
 * keyboard.c
 *
 *  Created on: Jun 12, 2013
 *      Author: CongDanh
 */


#include "keyboard.h"
#include <msp430.h>
#include <stdlib.h>

extern key_info_t *pFirst;
extern key_info_t *pLast;

void keyboard_init(){
	// TODO: Put code to initialize the keyboard.
	// all unused pins as outputs with low-level
    P1OUT = 0x00;
    P1DIR = 0xFF;
    // 2.1 to 2.6 is used for SPI
	P2OUT &= 0x5F;
    P2DIR |= 0xC0;
    P3OUT = 0xFF;
    P3DIR = 0xFF;
    P4OUT = 0x00;
    P4DIR = 0xFF;
    P5OUT = 0x00;
    P5DIR = 0xFF;
    P6OUT = BIT4;
    P6DIR = 0xFF;
	P7OUT = 0x00;
    P7DIR = 0xFF;
    P8OUT = 0x00;
    P8DIR = 0xFF;
	P9OUT = 0x00;
	P9DIR = 0xFF;
}

key_info_t *pFirst = NULL, *pLast = NULL;

unsigned char scan_key(){
	unsigned char keyPressed = 0;
	// Put code to scan key here.
	if(keyPressed){
		if(pFirst){
			pLast->pNext = malloc(sizeof(key_info_t));
			pLast = pLast->pNext;
		} else {
			pFirst = pLast = malloc(sizeof(key_info_t));
		}
		pLast->key = keyPressed;
		pLast->pNext = NULL;
	}
	return keyPressed;
}
