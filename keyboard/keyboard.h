/*
 * keyboard.h
 *
 *  Created on: Jun 12, 2013
 *      Author: CongDanh
 */

#ifndef KEYBOARD_H_
#define KEYBOARD_H_

#define SPI_KEYBOARD_TEST 18

typedef struct key_info_t key_info_t;

struct key_info_t{
	unsigned char key;
	key_info_t *pNext;
};


extern key_info_t *pFirst;

#ifdef SPI_KEYBOARD_TEST
extern key_info_t *pLast;
#endif

/**
 * Initialization of ports
 */
extern void keyboard_init();

extern unsigned char scan_key();

#endif /* KEYBOARD_H_ */
