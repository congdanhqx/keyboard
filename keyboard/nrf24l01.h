/*
 * nrf24l01.h
 *
 *  Created on: Jul 2, 2013
 *      Author: Cong Danh
 */

#ifndef NRF24L01_H_
#define NRF24L01_H_

#include "typedef.h"
/* START: Initial Function */
void	nrf24l01_init(nrf24l01_cb_t callback);

void    nrf24l01_set_rx_address(uint8_t *addr);

void    nrf24l01_set_tx_address(uint8_t *addr);
/* END: Initial Function */

/* START: Get State Function */
uint8_t	nrf24l01_rx_ready(void);

uint8_t	nrf24l01_get_fifo_status(void);

int8_t	nrf24l01_get_last_message_status(void);
/* END: Get State Function */

/* START: IO Function */
uint8_t	nrf24l01_write(uint8_t *data, int length);

uint8_t	nrf24l01_read(uint8_t *buffer, int len);
/* END: IO Function */

void	nrf24l01_to_rx(void);

void	nrf24l01_to_tx(void);

void	nrf24l01_power_down(void);

uint8_t	nrf24l01_get_config(void);

#endif /* NRF24L01_H_ */
