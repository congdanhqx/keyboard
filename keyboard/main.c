#include <msp430.h>

#include <stdint.h>
#include <stdlib.h>

#include "nrf24l01.h"
#include "keyboard.h"

/*
 * main.c
 */

uint8_t output = 0, input = 0;

#define COMPARE_VALUE 30000

inline void timer_init();
void nrf24l01_callback(uint8_t success);

int main(void) {
	WDTCTL = WDTPW | WDTHOLD;	// Stop watchdog timer
	keyboard_init();
	nrf24l01_init(&nrf24l01_callback);
	timer_init();
	while (1) {
		// TODO: Study the document, choose the most suitable LPM Mode.
		// Enter LPM0, enable interrupts
		__bis_SR_register(LPM0_bits + GIE);
		if (pFirst) {
			nrf24l01_write(&(pFirst->key), 1);
			key_info_t *pTemp = pFirst;
			pFirst = pFirst->pNext;
			free(pTemp);
		}
	}
}

void nrf24l01_callback(uint8_t success) {
	if (success) {
		P4OUT ^= BIT0; /* Transmission OK. */
	} else {
		P4OUT ^= BIT1; /* Transmission failed. */
	}
}

void timer_init() {
#ifdef SPI_KEYBOARD_TEST
	// For testing SPI purpose, use GPIO PORT1 Interrupt.
	P1OUT |= (BIT6);
	P1REN |= BIT6;
	P1IFG = 0;
	P1IE |= BIT6;
#else
	//Start timer in continuous mode sourced by SMCLK
	TIMER_A_configureContinuousMode(TIMER_A1_BASE, TIMER_A_CLOCKSOURCE_SMCLK,
			TIMER_A_CLOCKSOURCE_DIVIDER_1, TIMER_A_TAIE_INTERRUPT_DISABLE,
			TIMER_A_DO_CLEAR);

	// Initialize compare mode
	TIMER_A_clearCaptureCompareInterruptFlag(TIMER_A1_BASE,
			TIMER_A_CAPTURECOMPARE_REGISTER_0);
	TIMER_A_initCompare(TIMER_A1_BASE, TIMER_A_CAPTURECOMPARE_REGISTER_0,
			TIMER_A_CAPTURECOMPARE_INTERRUPT_ENABLE,
			TIMER_A_OUTPUTMODE_OUTBITVALUE, COMPARE_VALUE);

	TIMER_A_startCounter(TIMER_A1_BASE, TIMER_A_CONTINUOUS_MODE);

#endif
}

#ifdef SPI_KEYBOARD_TEST
#pragma vector=PORT1_VECTOR
__interrupt void port1_irq(void) {
	if (P1IFG & BIT6) {
		__delay_cycles(0x23FF);
		if (pFirst) {
			pLast->pNext = malloc(sizeof(key_info_t));
			pLast = pLast->pNext;
		} else {
			pFirst = pLast = malloc(sizeof(key_info_t));
		}
		pLast->key = SPI_KEYBOARD_TEST;
		pLast->pNext = NULL;
		__bic_SR_register_on_exit(LPM3_bits);
		P1IFG &= ~BIT6;
	}
}

#else
#pragma vector=TIMER1_A0_VECTOR
__interrupt void TIMER1_A0_ISR(void) {
	if (scan_key(pFirst)) {
		// TODO: Logic Code here.
		__bic_SR_register_on_exit(LPM3_bits);
	}

	//Add Offset to CCR0
	TIMER_A_setCompareValue(TIMER_A1_BASE, TIMER_A_CAPTURECOMPARE_REGISTER_0,
			COMPARE_VALUE);
}
#endif
