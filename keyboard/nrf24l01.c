/*
 * nrf24l01.c
 *
 *  Created on: Jul 2, 2013
 *      Author: Cong Danh
 */
#include "nrf24l01.h"

#include <msp430.h>
#include "spi.h"
#include "nrf24l01_register.h"

/**
 * Chip Select at P2.4
 *
 * MSP430 Output, Nordic Input
 */
#define RF_CSN BIT4
/**
 * Nordic Enable RX or TX Mode.
 *
 * MSP430 Output, Nordic Input
 */
#define RF_CE	BIT5
#define	RF_IN			P2IN
#define RF_OUT			P2OUT
#define	RF_DIR			P2DIR
#define RF_REN			P2REN
#define	RF_DS			P2DS
#define	RF_SEL			P2SEL
#define	RF_IES			P2IES
#define	RF_IE			P2IE
#define	RF_IFG			P2IFG
/**
 * Interrupt Request at P2.0
 *
 * MSP430 Input, Nordic Output
 */
#define NORDIC_IRQ 		BIT0
#define IRQ_VECTOR 		PORT2_VECTOR
#define	IRQ_IN			P2IN
#define IRQ_OUT			P2OUT
#define	IRQ_DIR			P2DIR
#define IRQ_REN			P2REN
#define	IRQ_DS			P2DS
#define	IRQ_SEL			P2SEL
#define	IRQ_IES			P2IES
#define	IRQ_IE			P2IE
#define	IRQ_IFG			P2IFG

// Forward declaration.

#define nrf24l01_write_register(reg,data,len)	nrf24l01_execute_command(W_REGISTER(reg), data, len, 0)
#define nrf24l01_read_register(reg,data,len)	nrf24l01_execute_command(R_REGISTER(reg), data, len, 1)
#define nrf24l01_flush_tx()						nrf24l01_execute_command(FLUSH_TX, NULL, 0, 1)
#define nrf24l01_flush_rx()						nrf24l01_execute_command(FLUSH_RX, NULL, 0, 1)
#define nrf24l01_reuse_tx_pl()					nrf24l01_execute_command(REUSE_TX_PL, NULL, 0, 1)
#define nrf24l01_nop()							nrf24l01_execute_command(NRF24L01_NOP, NULL, 0, 1)

void nrf24l01_set_csn(void);
void nrf24l01_clear_csn(void);
void nrf24l01_set_ce(void);
void nrf24l01_clear_ce(void);
uint8_t nrf24l01_config_register(uint8_t reg, uint8_t value);
uint8_t nrf24l01_execute_command(uint8_t instruction, uint8_t* data,
		unsigned int len, bool copydata);

void nrf24l01_spi_send_read(uint8_t * data, unsigned int len, bool copydata);
// End Forward Declaration.

nrf24l01_cb_t pCallback;

void nrf24l01_init(nrf24l01_cb_t callback) {

	spi_init();
	RF_DIR |= (RF_CSN | RF_CE);
	IRQ_OUT |= NORDIC_IRQ;
	IRQ_DIR &= ~NORDIC_IRQ;
	IRQ_REN |= NORDIC_IRQ;
	IRQ_IE = NORDIC_IRQ;

	pCallback = callback;

	// RF Channel to default
	nrf24l01_config_register(ADDR_RF_CH, RF_CH_DEFAULT);

	// Initial payload width
	nrf24l01_config_register(RX_PW_P0, RX_PW_DEFAULT);
	nrf24l01_config_register(RX_PW_P1, 1);
	nrf24l01_config_register(RX_PW_P2, RX_PW_DEFAULT);
	nrf24l01_config_register(RX_PW_P3, RX_PW_DEFAULT);
	nrf24l01_config_register(RX_PW_P4, RX_PW_DEFAULT);
	nrf24l01_config_register(RX_PW_P5, RX_PW_DEFAULT);

	// Setup RF
	nrf24l01_config_register(ADDR_RF_SETUP, RF_PWR_6 | RF_DR_1M);

	// Default configuration.
	nrf24l01_config_register(ADDR_CONFIG, CONFIG_DEFAULT);

	// Auto Ack: Default.
	nrf24l01_config_register(ADDR_EN_AA, EN_AA_DEFAULT);

	// Enable RX Address: Pipe 0 and Pipe 1, default.
	nrf24l01_config_register(ADDR_EN_RXADDR, ERX_DEFAULT);

	// Setup retransmit: Delay 1ms, up to 15 times retransmit.
	nrf24l01_config_register(ADDR_SETUP_RETR, 0x4F);

	// Set Address Width default (5 bytes --> use MSB).
	nrf24l01_config_register(ADDR_SETUP_AW, SETUP_AW_DEFAULT);

	// Use static payload length.
	nrf24l01_config_register(DYNPD, DYNPD_DEFAULT);

	nrf24l01_clear_csn();
	spi_xferchar(FLUSH_RX);
	nrf24l01_set_csn();

	nrf24l01_config_register(ADDR_STATUS, STATUS_ALL_IR);

	nrf24l01_clear_ce();
	nrf24l01_config_register(ADDR_CONFIG, CONFIG_PWR_RX);
	nrf24l01_set_ce();

	__delay_cycles(1500);

	// Clear Interrupt flags.
	IRQ_IFG = 0;
}
#pragma vector=PORT2_VECTOR
__interrupt void port2_irq() {
	if (IRQ_IFG & NORDIC_IRQ) {
		_delay_cycles(0x1FF);
		if (!(IRQ_IN & NORDIC_IRQ)) {
			nrf24l01_clear_csn();
			uint8_t msg_status = spi_xferchar(NRF24L01_NOP);
			nrf24l01_set_csn();

			(*pCallback)(msg_status & TX_DS);
		}
		IRQ_IFG = 0;
	}
}

void nrf24l01_set_rx_address(uint8_t *addr) {
	nrf24l01_clear_ce();
	nrf24l01_write_register(RX_ADDR_P1, addr, 5);
	nrf24l01_set_ce();
}

void nrf24l01_set_tx_address(uint8_t *addr) {
	nrf24l01_write_register(RX_ADDR_P0, addr, 5);
	nrf24l01_write_register(TX_ADDR, addr, 5);
}

uint8_t nrf24l01_rx_ready(void) {
	nrf24l01_clear_csn();
	uint8_t rx_status = spi_xferchar(NRF24L01_NOP);
	nrf24l01_set_csn();
	return (rx_status & RX_DR);
	/*	return 1;

	 nrf24l01_read_register(FIFO_STATUS, &rx_status, 1);
	 return !(rx_status & RX_EMPTY);*/
}

uint8_t nrf24l01_get_fifo_status(void) {
	uint8_t fifo_status;
	nrf24l01_read_register(FIFO_STATUS, &fifo_status, 1);
	return fifo_status;
}

int8_t nrf24l01_get_last_message_status(void) {
	nrf24l01_clear_csn();
	uint8_t msg_status = spi_xferchar(NRF24L01_NOP);
	nrf24l01_set_csn();

	if (msg_status & TX_DS) {
		return 0;
	}
	if (msg_status & MAX_RT) {
		return -1;
	}
	return 1;
}

uint8_t nrf24l01_write(uint8_t* data, int len) {
	uint8_t status_write;
	unsigned int i;
	// Switch to Standby I Mode.
	nrf24l01_clear_ce();
	nrf24l01_to_tx();
	/* Do we really need to flush TX FIFO each time ? */
#ifdef ALWAYS_FLUSH_TX
	/* Pull down chip select */
	nrf24l01_clear_csn();
	/* Write command to flush transmit FIFO */
	spi_xferchar(FLUSH_TX);
	/* Pull up chip select */
	nrf24l01_set_csn();
#endif
	nrf24l01_clear_csn();
	status_write = spi_xferchar(W_TX_PAYLOAD);
	for (i = 0; i < len; ++i) {
		spi_xferchar(data[i]);
	}
	nrf24l01_set_csn();
	// Start transmission.
	nrf24l01_set_ce();

	return status_write;
}

uint8_t nrf24l01_read(uint8_t* buffer, int len) {
	uint8_t read_status;
	unsigned int i;

	nrf24l01_clear_csn();
	read_status = spi_xferchar(R_RX_PAYLOAD);
	for (i = 0; i < len; ++i) {
		buffer[i] = spi_xferchar(NULL);
	}
	nrf24l01_set_csn();

	nrf24l01_config_register(ADDR_STATUS, RX_DR);

	return read_status;
}

void nrf24l01_to_rx(void) {
	nrf24l01_clear_csn();
	spi_xferchar(FLUSH_RX);
	nrf24l01_set_csn();

	nrf24l01_config_register(ADDR_STATUS, STATUS_ALL_IR);

	nrf24l01_clear_ce();
	nrf24l01_config_register(ADDR_CONFIG, CONFIG_PWR_RX);
	nrf24l01_set_ce();
}

void nrf24l01_to_tx(void) {
	nrf24l01_config_register(ADDR_STATUS, STATUS_ALL_IR);
	nrf24l01_config_register(ADDR_CONFIG, CONFIG_PWR_TX);
}

void nrf24l01_power_down(void) {
	nrf24l01_clear_ce();
	nrf24l01_config_register(ADDR_CONFIG, CONFIG_DEFAULT);
}

void nrf24l01_set_csn(void) {
	RF_OUT |= RF_CSN;
}

void nrf24l01_clear_csn(void) {
	RF_OUT &= ~RF_CSN;
}

void nrf24l01_set_ce(void) {
	RF_OUT |= RF_CE;
}

void nrf24l01_clear_ce(void) {
	RF_OUT &= ~RF_CE;
}

uint8_t nrf24l01_config_register(uint8_t reg, uint8_t value) {
	nrf24l01_clear_csn();
	uint8_t config_status = spi_xferchar(W_REGISTER(reg));
	spi_xferchar(value);
	nrf24l01_set_csn();
	__delay_cycles(_5MS);
	return config_status;
}

uint8_t nrf24l01_get_config(void) {
	nrf24l01_clear_csn();
	/*uint8_t config_status = */spi_xferchar(R_REGISTER(ADDR_CONFIG));
	uint8_t config_status = spi_xferchar(0xFF);
	nrf24l01_set_csn();
	return config_status;
}

uint8_t nrf24l01_execute_command(uint8_t instruction, uint8_t* data,
		unsigned int len, bool copydata) {
	uint8_t cmd_status;
	unsigned int i;

	nrf24l01_clear_csn();

	cmd_status = spi_xferchar(instruction);
	if (copydata) {
		for (i = 0; i < len; ++i) {
			data[i] = spi_xferchar(data[i]);
		}
	} else {
		for (i = 0; i < len; ++i) {
			spi_xferchar(data[i]);
		}
	}

	nrf24l01_set_csn();

	__delay_cycles(_130US);
	return cmd_status;
}
