/*
 * typedef.h
 *
 *  Created on: Jul 5, 2013
 *      Author: Cong Danh
 */

#ifndef TYPEDEF_H_
#define TYPEDEF_H_
#include <stdint.h>

typedef unsigned char bool;

typedef void (*nrf24l01_cb_t)(uint8_t success);


#endif /* TYPEDEF_H_ */
