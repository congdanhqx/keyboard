/*
 * spi.c
 *
 * This file implement for SPI.
 *
 *  Created on: Jun 8, 2013
 *      Author: CongDanh
 */

#include "spi.h"
#include <msp430.h>

/**
 * SPI Clock at P2.3
 *
 * MSP430 Output, Nordic Input
 */
#define SCLK BIT3
/**
 * SPI MISO (Slave Out, Master In) at P2.2
 *
 * MSP430 Input, Nordic Output
 */
#define MISO BIT2
/**
 * SPI MOSI (Slave In, Master Out) at P2.1
 *
 * MSP430 Output, Nordic Input
 */
#define MOSI BIT1

/**
 * Initializes the SPI Master block.
 */
void spi_init(void) {
	/**
	 * From TIs users manual
	 * http://www.ti.com/lit/ug/slau208m/slau208m.pdf
	 * Section 35.3.1
	 * The recommended USCI initialization/re-configuration process is:
	 * 1. Set UCSWRST (BIS.B #UCSWRST,&UCxCTL1)
	 * 2. Initialize all USCI registers with UCSWRST=1 (including UCxCTL1)
	 * 3. Configure ports
	 * 4. Clear UCSWRST via software (BIC.B #UCSWRST,&UCxCTL1) - Function spi_enable
	 * 5. Enable interrupts (optional) via UCxRXIE and/or UCxTXIE
	 */

	// (1) Disable the USCI Module.
	UCB0CTL1 |= UCSWRST;

	// (2)
	// Reset UCB0CTL0 values. See Table 35-15: UCBxCTL0 Register Description.
	UCB0CTL0 &= ~(UCCKPH | UCCKPL | UC7BIT | UCMSB);
	// Reset UCB0CTL1 values.
	UCB0CTL1 &= ~(UCSSEL_3);
	// Select Clock
	UCB0CTL1 |= UCSSEL__SMCLK;
	// Note in page 928 suggest using MSB-first mode for communicating with other interface.
	UCB0CTL0 |= UCMSB;
	// Mode: Master, 3pin, SPI.
	UCB0CTL0 |= UCMST | UCMODE_0 | UCSYNC | UCCKPH;

	// (3)
	P2SEL |= MISO | MOSI | SCLK;

	// (4)
	spi_enable();

	// (5)
	// Enable Interrupt, not needed.

}

void spi_enable(void) {
	UCB0CTL1 &= ~UCSWRST;
}

void spi_disable(void) {
	UCB0CTL1 |= UCSWRST;
}

unsigned char spi_xferchar(unsigned char c){
	UCB0TXBUF = c;
	while (!(UCB0IFG & UCTXIE));
	return UCB0RXBUF;
}
