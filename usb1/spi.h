/*
 * spi.h
 *
 *  Created on: Jun 8, 2013
 *      Author: CongDanh
 */

#ifndef SPI_H_
#define SPI_H_

/**
 * Initialize SPI Interface.
 **/
extern void spi_init(void);

/**
 * Enable SPI via UCSWRST.
 */
inline void spi_enable(void);

/**
 * Disable SPI via UCSWRST.
 */
inline void spi_disable(void);


extern unsigned char spi_xferchar(unsigned char c);

#endif /* SPI_H_ */
