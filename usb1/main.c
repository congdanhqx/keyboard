#include <msp430.h> 

#include <stdint.h>
#include <stdlib.h>

#include "nrf24l01.h"
/*
 * main.c
 */

void port_init(void);

int main(void) {
	WDTCTL = WDTPW | WDTHOLD;	// Stop watchdog timer
	port_init();
	nrf24l01_init();

	{
		uint8_t tx_address[5] = { 0x17, 0xD4, 0xDE, 0xAD, 0xCE };
		uint8_t rx_address[5] = { 0x43, 0xD1, 0xED, 0x11, 0x43 };
		nrf24l01_set_tx_address(tx_address);
		nrf24l01_set_rx_address(rx_address);
	}
	unsigned char buffer;
	_enable_interrupts();
	while (1) {
		// TODO: Study the document, choose the most suitable LPM Mode.
		// Enter LPM0, enable interrupts
		__bis_SR_register(LPM0_bits + GIE);
		nrf24l01_read(&buffer, 1);
		if (buffer == 18)
			P1OUT ^= BIT0;
		else
			P1OUT ^= BIT1;
	}
}

void port_init(void) {
	P1OUT = 0x00;
	P1DIR = 0xFF;
	// 2.1 to 2.6 is used for SPI
	P2OUT = 0x5F;
	P2DIR = 0xC0;
	P3OUT = 0xFF;
	P3DIR = 0xFF;
	P4OUT = 0xFF;
	P4DIR = 0xFF;
	P5OUT = 0x00;
	P5DIR = 0xFF;
	P6OUT = 0x00;
	P6DIR = 0xFF;
	P7OUT = 0x00;
	P7DIR = 0xFF;
	P8OUT = 0x00;
	P8DIR = 0xFF;
	P9OUT = 0x00;
	P9DIR = 0xFF;

	// For testing SPI purpose, use GPIO PORT1 Interrupt.
	P1OUT |= (BIT6);
	P1REN |= BIT6;
	P1IFG = 0;
	P1IE |= BIT6;
}
