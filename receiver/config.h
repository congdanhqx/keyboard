/*
 * config.h
 *
 *  Created on: 27-07-2013
 *      Author: CongDanh
 */

#ifndef CONFIG_H_
#define CONFIG_H_

/**
 * SPI Peripheral Port.
 */
#define SPI_SEL	P2SEL
/**
 * SPI Clock at P2.3
 *
 * MSP430 Output, Nordic Input
 */
#define SCLK BIT3
/**
 * SPI MISO (Slave Out, Master In) at P2.2
 *
 * MSP430 Input, Nordic Output
 */
#define MISO BIT2
/**
 * SPI MOSI (Slave In, Master Out) at P2.1
 *
 * MSP430 Output, Nordic Input
 */
#define MOSI BIT1


/**
 * Chip Select at P2.4
 *
 * MSP430 Output, Nordic Input
 */
#define RF_CSN BIT4
/**
 * Nordic Enable RX or TX Mode.
 *
 * MSP430 Output, Nordic Input
 */
#define RF_CE	BIT5
#define	RF_IN			P2IN
#define RF_OUT			P2OUT
#define	RF_DIR			P2DIR
#define RF_REN			P2REN
#define	RF_DS			P2DS
#define	RF_SEL			P2SEL
#define	RF_IES			P2IES
#define	RF_IE			P2IE
#define	RF_IFG			P2IFG
/**
 * Interrupt Request at P2.0
 *
 * MSP430 Input, Nordic Output
 */
#define NORDIC_IRQ 		BIT0
#define IRQ_VECTOR 		PORT2_VECTOR
#define	IRQ_IN			P2IN
#define IRQ_OUT			P2OUT
#define	IRQ_DIR			P2DIR
#define IRQ_REN			P2REN
#define	IRQ_DS			P2DS
#define	IRQ_SEL			P2SEL
#define	IRQ_IES			P2IES
#define	IRQ_IE			P2IE
#define	IRQ_IFG			P2IFG

#define RF_TX_ADDR { 0x17, 0xD4, 0xDE, 0xAD, 0xCE }
#define RF_RX_ADDR { 0x43, 0xD1, 0xED, 0x11, 0x43 }

#endif /* CONFIG_H_ */
