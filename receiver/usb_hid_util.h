#include <descriptors.h>
#include <USB_API/USB_Common/device.h>

#ifdef _HID_
/**
 * Write HID command in buffer and wait till done.
 *
 * @param buffer	the buffer contains the command.
 * @param size		size of buffer.
 * @param intfNum
 */
BYTE hid_write (
		BYTE *buffer,
		WORD size,
		BYTE intfNum,
		ULONG ulTimeout);
BYTE hid_write_async (
		BYTE *buffer,
		WORD size,
		BYTE intfNum,
		ULONG ulTimeout);
WORD hid_read(
		BYTE *buffer,
		WORD size,
		BYTE intfNum);
#endif // _HID_
