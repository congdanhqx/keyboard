/*
 * typedef.h
 *
 *  Created on: 29-07-2013
 *      Author: CongDanh
 */

#ifndef TYPEDEF_H_
#define TYPEDEF_H_
#include <stdint.h>

/*! 8-bit register definition */
typedef uint8_t register_t;

typedef uint8_t key_t;

typedef union {
	struct {
		uint32_t header;
		uint16_t crc;
		uint8_t updated;
		register_t empty_slots;
		union {
			uint16_t data[4];
			key_t keys[8];
		} keys;
	} report;
	uint8_t data[16];
} report_t;

#endif /* TYPEDEF_H_ */
