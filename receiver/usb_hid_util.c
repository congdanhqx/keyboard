#include "usb_hid_util.h"
#include <USB_API/USB_Common/device.h>
#include <USB_API/USB_Common/types.h>   //Basic Type declarations

#include <USB_API/USB_Common/usb.h>     //USB-specific functions

#ifdef _HID_
    #include <USB_API/USB_HID_API/UsbHid.h>
#endif

#include <intrinsics.h>

#ifdef _HID_

BYTE hid_write (BYTE* dataBuf,
    WORD size,
    BYTE intfNum,
    ULONG ulTimeout)
{
    ULONG sendCounter = 0;
    WORD bytesSent, bytesReceived;

    switch (USBHID_sendData(dataBuf,size,intfNum)){
        case kUSBHID_sendStarted:
            break;
        case kUSBHID_busNotAvailable:
            return ( 2) ;
        case kUSBHID_intfBusyError:
            return ( 3) ;
        case kUSBHID_generalError:
            return ( 4) ;
        default:;
    }

    /* If execution reaches this point, then the operation successfully started.  Now wait til it's finished. */
    while (1){
        BYTE ret = USBHID_intfStatus(intfNum,&bytesSent,&bytesReceived);
        if (ret & kUSBHID_busNotAvailable){                 /* This may happen at any time */
            return ( 2) ;
        }
        if (ret & kUSBHID_waitingForSend){
            if (ulTimeout && (sendCounter++ >= ulTimeout)){ /* Incr counter & try again */
                return ( 1) ;                               /* Timed out */
            }
        } else {
            return ( 0) ;                                   /* If neither busNotAvailable nor waitingForSend, it succeeded */
        }
    }
}

BYTE hid_send_data_async (BYTE* dataBuf,
    WORD size,
    BYTE intfNum,
    ULONG ulTimeout)
{
    ULONG sendCounter = 0;
    WORD bytesSent, bytesReceived;

    while (USBHID_intfStatus(intfNum,&bytesSent,
               &bytesReceived) & kUSBHID_waitingForSend){
        if (ulTimeout && ((sendCounter++) > ulTimeout)){    /* A send operation is underway; incr counter & try again */
            return ( 1) ;                                   /* Timed out */
        }
    }

    /* The interface is now clear.  Call sendData(). */
    switch (USBHID_sendData(dataBuf,size,intfNum)){
        case kUSBHID_sendStarted:
            return ( 0) ;
        case kUSBHID_busNotAvailable:
            return ( 2) ;
        default:
            return ( 4) ;
    }
}

WORD hid_read (BYTE* dataBuf, WORD size, BYTE intfNum)
{
    WORD bytesInBuf;
	WORD rxCount = 0;
    BYTE* currentPos = dataBuf;

    while (bytesInBuf = USBHID_bytesInUSBBuffer(intfNum)){
        if ((WORD)(currentPos - dataBuf + bytesInBuf) <= size){
            rxCount = bytesInBuf;
			USBHID_receiveData(currentPos,rxCount,intfNum);
        	currentPos += rxCount;
        } else {
            rxCount = size - (currentPos - dataBuf);
			USBHID_receiveData(currentPos,rxCount,intfNum);
        	currentPos += rxCount;
			return (currentPos - dataBuf);
        }
    }
	
	return (currentPos - dataBuf);
}

#endif // _HID_
