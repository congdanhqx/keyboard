/*
 * spi.c
 *
 * This file implement for SPI.
 *
 *  Created on: Jun 8, 2013
 *      Author: CongDanh
 */

#include "spi.h"
#include <msp430.h>

#include "config.h"

/**
 * Initializes the SPI Master block.
 */
void spi_init(void) {
	/**
	 * From TIs users manual
	 * http://www.ti.com/lit/ug/slau208m/slau208m.pdf
	 * Section 35.3.1
	 * The recommended USCI initialization/re-configuration process is:
	 * 1. Set UCSWRST (BIS.B #UCSWRST,&UCxCTL1)
	 * 2. Initialize all USCI registers with UCSWRST=1 (including UCxCTL1)
	 * 3. Configure ports
	 * 4. Clear UCSWRST via software (BIC.B #UCSWRST,&UCxCTL1) - Function spi_enable
	 * 5. Enable interrupts (optional) via UCxRXIE and/or UCxTXIE
	 */

	// (1) Disable the USCI Module.
	UCB0CTL1 |= UCSWRST;

	// (2)
	// Reset UCB0CTL0 values. See Table 35-15: UCBxCTL0 Register Description.
	UCB0CTL0 &= ~(UCCKPH | UCCKPL | UC7BIT | UCMSB);
	// Reset UCB0CTL1 values.
	UCB0CTL1 &= ~(UCSSEL_3);
	// Select Clock
	UCB0CTL1 |= UCSSEL__SMCLK;
	/**
	 * Note in page 928 suggest using MSB-first mode for communicating with other interface.
	 * In nRF24L01 module, we have to enable MSB mode too.
	 *
	 * Mode: Master, 3pin, SPI.
	 *
	 * According figure 26, 27 of nRF24L01+ specification (page 52)
	 * and Figure 35-4 of MSP430F6659 specification.
	 * Set: UCCKPH (clock phase) high and UCCKPL (clock polarity inactivity) low.
	 */
	UCB0CTL0 |= UCMSB | UCMST | UCMODE_0 | UCSYNC | UCCKPH;

	/**
	 * (3)
	 * Configure Peripheral Port.
	 */
	SPI_SEL |= MISO | MOSI | SCLK;

	// (4)
	UCB0CTL1 &= ~UCSWRST;

	// (5)
	// Enable Interrupt, not needed.

}

unsigned char spi_xferchar(unsigned char c) {
	UCB0TXBUF = c;
	while (!(UCB0IFG & UCTXIE));
	return UCB0RXBUF;
}

#pragma vector=USCI_B0_VECTOR
__interrupt void usci_b0_irq(void){
	// Do nothing, just trap the interrupt.
}
