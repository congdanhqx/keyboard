/*
 * usb_shared.h
 *
 *  Created on: 09-08-2013
 *      Author: CongDanh
 */

#ifndef USB_SHARED_H_
#define USB_SHARED_H_

#define SYNC_CAP_NUM_LCK

/**
 * Indicate that USB module had received data from host computer or not.
 *
 * @value	1 Data Received.
 * 			0 otherwise.
 */
extern uint8_t usb_hid_is_data_received;


#endif /* USB_SHARED_H_ */
