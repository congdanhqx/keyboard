/*
 * nrf24l01_register.h
 *
 *  Created on: 18-07-2013
 *      Author: CongDanh
 */

#ifndef NRF24L01_REGISTER_H_
#define NRF24L01_REGISTER_H_

#ifndef NULL
#define NULL 0
#endif

#define ALWAYS_FLUSH_TX

#define _5MS		20000
#define _2MS		8000
#define _130US		520
#define _15US		60
/**
 * The following are defines for all of the commands and data masks on the SPI interfaces.
 *
 * See nRF24L01 Products Specification, Table 20, page 51.
 */
#define R_REGISTER(reg)	(reg) /* 0x00 | reg */
#define W_REGISTER(reg)	(0x20 | reg)
#define R_RX_PAYLOAD	0x61
#define W_TX_PAYLOAD	0xA0
#define FLUSH_TX		0xE1
#define FLUSH_RX		0xE2
#define REUSE_TX_PL		0xE3
#define NRF24L01_NOP	0xFF

/*
 * Register map table.
 *
 * See nRF24L01 Products Specification, section 9.1, page 57.
 */
#define ADDR_CONFIG			0x00
#define CONFIG_MASK_RX_DR	0x40
#define CONFIG_MASK_TX_DS	0x20
#define CONFIG_MASK_MAX_RT	0x10
#define CONFIG_EN_CRC		0x08
#define CONFIG_CRCO			0x04
#define CONFIG_PWR_UP		0x02
#define CONFIG_PRIM_RX		0x01
#define CONFIG_PWR_DOWN		CONFIG_EN_CRC
#define	CONFIG_PWR_TX		0x0A /* CONFIG_EN_CRC | CONFIG_PWR_UP */
#define	CONFIG_PWR_RX		0x0B /* CONFIG_EN_CRC | CONFIG_PWR_UP | CONFIG_PRIM_RX r*/
#define CONFIG_DEFAULT		CONFIG_EN_CRC

#define ADDR_EN_AA			0x01
#define EN_AA_ALL			0x3F
#define EN_AA_P5			0x20
#define EN_AA_P4			0x10
#define EN_AA_P3			0x08
#define EN_AA_P2			0x04
#define EN_AA_P1			0x02
#define EN_AA_P0			0x01
#define EN_AA_NONE			0x00
#define EN_AA_DEFAULT		EN_AA_ALL

#define ADDR_EN_RXADDR		0x02
#define ERX_ALL				0x3F
#define ERX_P5				0x20
#define ERX_P4				0x10
#define ERX_P3				0x08
#define ERX_P2				0x04
#define ERX_P1				0x02
#define ERX_P0				0x01
#define ERX_NONE			0x00
#define ERX_DEFAULT			0x03

#define ADDR_SETUP_AW		0x03
#define SETUP_AW_5BYTES		0x03
#define SETUP_AW_4BYTES		0x02
#define SETUP_AW_3BYTES		0x01
#define SETUP_AW_ILLEGAL	0x00
#define SETUP_AW_DEFAULT	SETUP_AW_5BYTES

#define ADDR_SETUP_RETR		0x04
#define SETUP_RETR_DEFAULT	0x03
/* Begin Declare Auto Retransmission Delay. */
#define ARD_4000			0xF0
#define ARD_3750			0xE0
#define ARD_3500			0xD0
#define ARD_3250			0xC0
#define ARD_3000			0xB0
#define ARD_2750			0xA0
#define ARD_2500			0x90
#define ARD_2250			0x80
#define ARD_2000			0x70
#define ARD_1750			0x60
#define ARD_1500			0x50
#define ARD_1250			0x40
#define ARD_1000			0x30
#define ARD_750				0x20
#define ARD_500				0x10
#define ARD_250				0x00
#define ARD_DEFAULT			0x00
/* End Declare Auto Retransmission Delay. */
/* Begin Declare Auto Retransmission Count. */
#define ARC_15				0x0F
#define ARC_14				0x0E
#define ARC_13				0x0D
#define ARC_12				0x0C
#define ARC_11				0x0B
#define ARC_10				0x0A
#define ARC_9				0x09
#define ARC_8				0x08
#define ARC_7				0x07
#define ARC_6				0x06
#define ARC_5				0x05
#define ARC_4				0x04
#define ARC_3				0x03
#define ARC_2				0x02
#define ARC_1				0x01
#define ARC_0				0x00
#define ARC_DEFAULT			0x03
/* End Declare Auto Retransmission Count. */

#define ADDR_RF_CH			0x05
#define RF_CH_DEFAULT		0x02

#define ADDR_RF_SETUP		0x06
#define CONT_WAVE			0x80
#define RF_DR_LOW			0x20
#define	PLL_LOCK			0x10
#define	RF_DR_250K			RF_DR_LOW
#define RF_DR_1M			0x00
#define RF_DR_2M			0x08
#define RF_PWR_0			0x06
#define	RF_PWR_6			0x04
#define RF_PWR_12			0x02
#define RF_PWR_18			0x00
#define RF_SETUP_DEFAULT	0x0E

#define ADDR_STATUS			0x07
#define RX_DR				0x40
#define TX_DS				0x20
#define MAX_RT				0x10
#define STATUS_ALL_IR		0x70
#define RX_P_NO_0			0x00
#define RX_P_NO_1			0x02
#define RX_P_NO_2			0x04
#define	RX_P_NO_3			0x06
#define RX_P_NO_4			0x08
#define RX_P_NO_5			0x0A
#define RX_FIFO_EMPTY		0x0E
#define STATUS_DEFAULT		0x0E

#define RX_ADDR_P0			0x0A
#define RX_ADDR_P0_DEFAULT	0xE7

#define RX_ADDR_P1			0x0B
#define RX_ADDR_P1_DEFAULT	0xC2

#define RX_ADDR_P2			0x0C
#define RX_ADDR_P2_DEFAULT	0xC3

#define RX_ADDR_P3			0x0D
#define RX_ADDR_P3_DEFAULT	0xC4

#define RX_ADDR_P4			0x0E
#define RX_ADDR_P4_DEFAULT	0xC5

#define RX_ADDR_P5			0x0F
#define RX_ADDR_P5_DEFAULT	0xC6

#define TX_ADDR				0x10
#define TX_DEFAULT			0xE7

#define TX_RX_MAIN			0xE7
#define TX_RX_SLAVE			0xD7

#define RX_PW_P0			0x11
#define RX_PW_P1			0x12
#define RX_PW_P2			0x13
#define RX_PW_P3			0x14
#define RX_PW_P4			0x15
#define RX_PW_P5			0x16
#define RX_PW_DEFAULT		0x00

#define FIFO_STATUS			0x17
#define TX_REUSE			0x40
#define TX_FULL				0x20
#define	TX_EMPTY			0x10
#define RX_FULL				0x02
#define	RX_EMPTY			0x01
#define	FIFO_DEFAULT		0x11

#define DYNPD				0x1C
#define DYNPD_DEFAULT		0x00

#endif /* NRF24L01_REGISTER_H_ */
