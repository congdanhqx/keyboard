/*
 * aes.c
 *
 *  Created on: 06-08-2013
 *      Author: CongDanh
 */
#include "aes.h"
#include <msp430.h>

#include <stdint.h>

#pragma DATA_SECTION(aes_matrix, ".aes_matrix")
#pragma DATA_ALIGN(aes_matrix, 256)
const uint8_t aes_matrix[256] =   {
//0     1    2      3     4    5     6     7      8    9     A      B    C     D     E     F
0x63, 0x7c, 0x77, 0x7b, 0xf2, 0x6b, 0x6f, 0xc5, 0x30, 0x01, 0x67, 0x2b, 0xfe, 0xd7, 0xab, 0x76, //0
0xca, 0x82, 0xc9, 0x7d, 0xfa, 0x59, 0x47, 0xf0, 0xad, 0xd4, 0xa2, 0xaf, 0x9c, 0xa4, 0x72, 0xc0, //1
0xb7, 0xfd, 0x93, 0x26, 0x36, 0x3f, 0xf7, 0xcc, 0x34, 0xa5, 0xe5, 0xf1, 0x71, 0xd8, 0x31, 0x15, //2
0x04, 0xc7, 0x23, 0xc3, 0x18, 0x96, 0x05, 0x9a, 0x07, 0x12, 0x80, 0xe2, 0xeb, 0x27, 0xb2, 0x75, //3
0x09, 0x83, 0x2c, 0x1a, 0x1b, 0x6e, 0x5a, 0xa0, 0x52, 0x3b, 0xd6, 0xb3, 0x29, 0xe3, 0x2f, 0x84, //4
0x53, 0xd1, 0x00, 0xed, 0x20, 0xfc, 0xb1, 0x5b, 0x6a, 0xcb, 0xbe, 0x39, 0x4a, 0x4c, 0x58, 0xcf, //5
0xd0, 0xef, 0xaa, 0xfb, 0x43, 0x4d, 0x33, 0x85, 0x45, 0xf9, 0x02, 0x7f, 0x50, 0x3c, 0x9f, 0xa8, //6
0x51, 0xa3, 0x40, 0x8f, 0x92, 0x9d, 0x38, 0xf5, 0xbc, 0xb6, 0xda, 0x21, 0x10, 0xff, 0xf3, 0xd2, //7
0xcd, 0x0c, 0x13, 0xec, 0x5f, 0x97, 0x44, 0x17, 0xc4, 0xa7, 0x7e, 0x3d, 0x64, 0x5d, 0x19, 0x73, //8
0x60, 0x81, 0x4f, 0xdc, 0x22, 0x2a, 0x90, 0x88, 0x46, 0xee, 0xb8, 0x14, 0xde, 0x5e, 0x0b, 0xdb, //9
0xe0, 0x32, 0x3a, 0x0a, 0x49, 0x06, 0x24, 0x5c, 0xc2, 0xd3, 0xac, 0x62, 0x91, 0x95, 0xe4, 0x79, //A
0xe7, 0xc8, 0x37, 0x6d, 0x8d, 0xd5, 0x4e, 0xa9, 0x6c, 0x56, 0xf4, 0xea, 0x65, 0x7a, 0xae, 0x08, //B
0xba, 0x78, 0x25, 0x2e, 0x1c, 0xa6, 0xb4, 0xc6, 0xe8, 0xdd, 0x74, 0x1f, 0x4b, 0xbd, 0x8b, 0x8a, //C
0x70, 0x3e, 0xb5, 0x66, 0x48, 0x03, 0xf6, 0x0e, 0x61, 0x35, 0x57, 0xb9, 0x86, 0xc1, 0x1d, 0x9e, //D
0xe1, 0xf8, 0x98, 0x11, 0x69, 0xd9, 0x8e, 0x94, 0x9b, 0x1e, 0x87, 0xe9, 0xce, 0x55, 0x28, 0xdf, //E
0x8c, 0xa1, 0x89, 0x0d, 0xbf, 0xe6, 0x42, 0x68, 0x41, 0x99, 0x2d, 0x0f, 0xb0, 0x54, 0xbb, 0x16 }; //F

#pragma DATA_SECTION(aes_inverse, ".aes_inverse")
#pragma DATA_ALIGN(aes_inverse, 256)
const unsigned char aes_inverse[256] =
{ 0x52, 0x09, 0x6a, 0xd5, 0x30, 0x36, 0xa5, 0x38, 0xbf, 0x40, 0xa3, 0x9e, 0x81, 0xf3, 0xd7, 0xfb
, 0x7c, 0xe3, 0x39, 0x82, 0x9b, 0x2f, 0xff, 0x87, 0x34, 0x8e, 0x43, 0x44, 0xc4, 0xde, 0xe9, 0xcb
, 0x54, 0x7b, 0x94, 0x32, 0xa6, 0xc2, 0x23, 0x3d, 0xee, 0x4c, 0x95, 0x0b, 0x42, 0xfa, 0xc3, 0x4e
, 0x08, 0x2e, 0xa1, 0x66, 0x28, 0xd9, 0x24, 0xb2, 0x76, 0x5b, 0xa2, 0x49, 0x6d, 0x8b, 0xd1, 0x25
, 0x72, 0xf8, 0xf6, 0x64, 0x86, 0x68, 0x98, 0x16, 0xd4, 0xa4, 0x5c, 0xcc, 0x5d, 0x65, 0xb6, 0x92
, 0x6c, 0x70, 0x48, 0x50, 0xfd, 0xed, 0xb9, 0xda, 0x5e, 0x15, 0x46, 0x57, 0xa7, 0x8d, 0x9d, 0x84
, 0x90, 0xd8, 0xab, 0x00, 0x8c, 0xbc, 0xd3, 0x0a, 0xf7, 0xe4, 0x58, 0x05, 0xb8, 0xb3, 0x45, 0x06
, 0xd0, 0x2c, 0x1e, 0x8f, 0xca, 0x3f, 0x0f, 0x02, 0xc1, 0xaf, 0xbd, 0x03, 0x01, 0x13, 0x8a, 0x6b
, 0x3a, 0x91, 0x11, 0x41, 0x4f, 0x67, 0xdc, 0xea, 0x97, 0xf2, 0xcf, 0xce, 0xf0, 0xb4, 0xe6, 0x73
, 0x96, 0xac, 0x74, 0x22, 0xe7, 0xad, 0x35, 0x85, 0xe2, 0xf9, 0x37, 0xe8, 0x1c, 0x75, 0xdf, 0x6e
, 0x47, 0xf1, 0x1a, 0x71, 0x1d, 0x29, 0xc5, 0x89, 0x6f, 0xb7, 0x62, 0x0e, 0xaa, 0x18, 0xbe, 0x1b
, 0xfc, 0x56, 0x3e, 0x4b, 0xc6, 0xd2, 0x79, 0x20, 0x9a, 0xdb, 0xc0, 0xfe, 0x78, 0xcd, 0x5a, 0xf4
, 0x1f, 0xdd, 0xa8, 0x33, 0x88, 0x07, 0xc7, 0x31, 0xb1, 0x12, 0x10, 0x59, 0x27, 0x80, 0xec, 0x5f
, 0x60, 0x51, 0x7f, 0xa9, 0x19, 0xb5, 0x4a, 0x0d, 0x2d, 0xe5, 0x7a, 0x9f, 0x93, 0xc9, 0x9c, 0xef
, 0xa0, 0xe0, 0x3b, 0x4d, 0xae, 0x2a, 0xf5, 0xb0, 0xc8, 0xeb, 0xbb, 0x3c, 0x83, 0x53, 0x99, 0x61
, 0x17, 0x2b, 0x04, 0x7e, 0xba, 0x77, 0xd6, 0x26, 0xe1, 0x69, 0x14, 0x63, 0x55, 0x21, 0x0c, 0x7d };

/**
 * AES Expanded key. This array is generated via another program.
 */
#pragma DATA_SECTION(aes_x_key, ".aes_x_key")
#pragma DATA_ALIGN(aes_x_key, 256)
const uint8_t aes_x_key[176] = {
0x84, 0x9d, 0x4f, 0x65, 0x87, 0x9c, 0xe9, 0xd3, 0xf2, 0x65, 0xf8, 0xab, 0xfa, 0xbd, 0xde, 0xad,
0xff, 0x80, 0xda, 0x48, 0x78, 0x1c, 0x33, 0x9b, 0x8a, 0x79, 0xcb, 0x30, 0x70, 0xc4, 0x15, 0x9d,
0xe1, 0xd9, 0x84, 0x19, 0x99, 0xc5, 0xb7, 0x82, 0x13, 0xbc, 0x7c, 0xb2, 0x63, 0x78, 0x69, 0x2f,
0x59, 0x20, 0x91, 0xe2, 0xc0, 0xe5, 0x26, 0x60, 0xd3, 0x59, 0x5a, 0xd2, 0xb0, 0x21, 0x33, 0xfd,
0xac, 0xe3, 0xc5, 0x5, 0x6c, 0x6, 0xe3, 0x65, 0xbf, 0x5f, 0xb9, 0xb7, 0xf, 0x7e, 0x8a, 0x4a,
0x4f, 0x9d, 0x13, 0x73, 0x23, 0x9b, 0xf0, 0x16, 0x9c, 0xc4, 0x49, 0xa1, 0x93, 0xba, 0xc3, 0xeb,
0x9b, 0xb3, 0xfa, 0xaf, 0xb8, 0x28, 0xa, 0xb9, 0x24, 0xec, 0x43, 0x18, 0xb7, 0x56, 0x80, 0xf3,
0x6a, 0x7e, 0xf7, 0x6, 0xd2, 0x56, 0xfd, 0xbf, 0xf6, 0xba, 0xbe, 0xa7, 0x41, 0xec, 0x3e, 0x54,
0x24, 0xcc, 0xd7, 0x85, 0xf6, 0x9a, 0x2a, 0x3a, 0x0, 0x20, 0x94, 0x9d, 0x41, 0xcc, 0xaa, 0xc9,
0x74, 0x60, 0xa, 0x6, 0x82, 0xfa, 0x20, 0x3c, 0x82, 0xda, 0xb4, 0xa1, 0xc3, 0x16, 0x1e, 0x68,
0x5, 0x12, 0x4f, 0x28, 0x87, 0xe8, 0x6f, 0x14, 0x5, 0x32, 0xdb, 0xb5, 0xc6, 0x24, 0xc5, 0xdd,
};

/**
 * Multiply by 2 in the galois field.
 */
uint8_t aes_galois_mul2(uint8_t value)
{
	if (value>>7)
	{
		value = value << 1;
		return (value^0x1b);
	} else
		return value<<1;
}

/**
 * Encrypt 128 bit data by the AES-128 algorithm.
 *
 * @param input	16 bytes plain data.
 * @param output output buffer for encrypted data, must be allocated at least 16 bytes.
 */
void aes_encrypt(uint8_t* input, uint8_t* output){
	uint8_t buf1, buf2, buf3, round, base;
	for(round = 16; round;){
		--round;
		output[round] = input[round];
	}
	// First 9 round.
	for (round = 0; round < 9; ++round) {
		base = round << 4;
		// AddRoundKey, SubBytes, ShiftRows.
		// row 0
		output[0] = aes_matrix[(output[0] ^ aes_x_key[base])];
		output[4] = aes_matrix[(output[4] ^ aes_x_key[base + 4])];
		output[8] = aes_matrix[(output[8] ^ aes_x_key[base + 8])];
		output[12] = aes_matrix[(output[12] ^ aes_x_key[base + 12])];
		// row 1
		buf1 = output[1] ^ aes_x_key[base + 1];
		output[1] = aes_matrix[(output[5] ^ aes_x_key[base + 5])];
		output[5] = aes_matrix[(output[9] ^ aes_x_key[base + 9])];
		output[9] = aes_matrix[(output[13] ^ aes_x_key[base + 13])];
		output[13] = aes_matrix[buf1];
		// row 2
		buf1 = output[2] ^ aes_x_key[base + 2];
		buf2 = output[6] ^ aes_x_key[base + 6];
		output[2] = aes_matrix[(output[10] ^ aes_x_key[base + 10])];
		output[6] = aes_matrix[(output[14] ^ aes_x_key[base + 14])];
		output[10] = aes_matrix[buf1];
		output[14] = aes_matrix[buf2];
		// row 3
		buf1 = output[15] ^ aes_x_key[base + 15];
		output[15] = aes_matrix[(output[11] ^ aes_x_key[base + 11])];
		output[11] = aes_matrix[(output[7] ^ aes_x_key[base + 7])];
		output[7] = aes_matrix[(output[3] ^ aes_x_key[base + 3])];
		output[3] = aes_matrix[buf1];

		// MixColumns.
		// col1
		buf1 = output[0] ^ output[1] ^ output[2] ^ output[3];
		buf2 = output[0];
		buf3 = output[0] ^ output[1];
		buf3 = aes_galois_mul2(buf3);
		output[0] = output[0] ^ buf3 ^ buf1;
		buf3 = output[1] ^ output[2];
		buf3 = aes_galois_mul2(buf3);
		output[1] = output[1] ^ buf3 ^ buf1;
		buf3 = output[2] ^ output[3];
		buf3 = aes_galois_mul2(buf3);
		output[2] = output[2] ^ buf3 ^ buf1;
		buf3 = output[3] ^ buf2;
		buf3 = aes_galois_mul2(buf3);
		output[3] = output[3] ^ buf3 ^ buf1;
		// col2
		buf1 = output[4] ^ output[5] ^ output[6] ^ output[7];
		buf2 = output[4];
		buf3 = output[4] ^ output[5];
		buf3 = aes_galois_mul2(buf3);
		output[4] = output[4] ^ buf3 ^ buf1;
		buf3 = output[5] ^ output[6];
		buf3 = aes_galois_mul2(buf3);
		output[5] = output[5] ^ buf3 ^ buf1;
		buf3 = output[6] ^ output[7];
		buf3 = aes_galois_mul2(buf3);
		output[6] = output[6] ^ buf3 ^ buf1;
		buf3 = output[7] ^ buf2;
		buf3 = aes_galois_mul2(buf3);
		output[7] = output[7] ^ buf3 ^ buf1;
		// col3
		buf1 = output[8] ^ output[9] ^ output[10] ^ output[11];
		buf2 = output[8];
		buf3 = output[8] ^ output[9];
		buf3 = aes_galois_mul2(buf3);
		output[8] = output[8] ^ buf3 ^ buf1;
		buf3 = output[9] ^ output[10];
		buf3 = aes_galois_mul2(buf3);
		output[9] = output[9] ^ buf3 ^ buf1;
		buf3 = output[10] ^ output[11];
		buf3 = aes_galois_mul2(buf3);
		output[10] = output[10] ^ buf3 ^ buf1;
		buf3 = output[11] ^ buf2;
		buf3 = aes_galois_mul2(buf3);
		output[11] = output[11] ^ buf3 ^ buf1;
		// col4
		buf1 = output[12] ^ output[13] ^ output[14] ^ output[15];
		buf2 = output[12];
		buf3 = output[12] ^ output[13];
		buf3 = aes_galois_mul2(buf3);
		output[12] = output[12] ^ buf3 ^ buf1;
		buf3 = output[13] ^ output[14];
		buf3 = aes_galois_mul2(buf3);
		output[13] = output[13] ^ buf3 ^ buf1;
		buf3 = output[14] ^ output[15];
		buf3 = aes_galois_mul2(buf3);
		output[14] = output[14] ^ buf3 ^ buf1;
		buf3 = output[15] ^ buf2;
		buf3 = aes_galois_mul2(buf3);
		output[15] = output[15] ^ buf3 ^ buf1;
	}
	// The 10th round (no MixColumns step in this round).
	// row 0
	output[0] = aes_matrix[(output[0] ^ aes_x_key[9 * 16])];
	output[4] = aes_matrix[(output[4] ^ aes_x_key[9 * 16 + 4])];
	output[8] = aes_matrix[(output[8] ^ aes_x_key[9 * 16 + 8])];
	output[12] = aes_matrix[(output[12] ^ aes_x_key[9 * 16 + 12])];
	// row 1
	buf1 = output[1] ^ aes_x_key[9 * 16 + 1];
	output[1] = aes_matrix[(output[5] ^ aes_x_key[9 * 16 + 5])];
	output[5] = aes_matrix[(output[9] ^ aes_x_key[9 * 16 + 9])];
	output[9] = aes_matrix[(output[13] ^ aes_x_key[9 * 16 + 13])];
	output[13] = aes_matrix[buf1];
	// row 2
	buf1 = output[2] ^ aes_x_key[9 * 16 + 2];
	buf2 = output[6] ^ aes_x_key[9 * 16 + 6];
	output[2] = aes_matrix[(output[10] ^ aes_x_key[9 * 16 + 10])];
	output[6] = aes_matrix[(output[14] ^ aes_x_key[9 * 16 + 14])];
	output[10] = aes_matrix[buf1];
	output[14] = aes_matrix[buf2];
	// row 3
	buf1 = output[15] ^ aes_x_key[9 * 16 + 15];
	output[15] = aes_matrix[(output[11] ^ aes_x_key[9 * 16 + 11])];
	output[11] = aes_matrix[(output[7] ^ aes_x_key[9 * 16 + 7])];
	output[7] = aes_matrix[(output[3] ^ aes_x_key[9 * 16 + 3])];
	output[3] = aes_matrix[buf1];

	// The last AddRoundKey step.
	output[0] ^= aes_x_key[160];
	output[1] ^= aes_x_key[161];
	output[2] ^= aes_x_key[162];
	output[3] ^= aes_x_key[163];
	output[4] ^= aes_x_key[164];
	output[5] ^= aes_x_key[165];
	output[6] ^= aes_x_key[166];
	output[7] ^= aes_x_key[167];
	output[8] ^= aes_x_key[168];
	output[9] ^= aes_x_key[169];
	output[10] ^= aes_x_key[170];
	output[11] ^= aes_x_key[171];
	output[12] ^= aes_x_key[172];
	output[13] ^= aes_x_key[173];
	output[14] ^= aes_x_key[174];
	output[15] ^= aes_x_key[175];
}

/**
 * Decrypt 128 bit data by the AES-128 algorithm.
 *
 * @param input	16 bytes encrypted data.
 * @param output output buffer for plain text, must be allocated at least 16 bytes.
 */
void aes_decrypt(uint8_t* input, uint8_t* output){
	uint8_t buf1, buf2, buf3, round, base;
	// The initial AddRoundKey step.
	output[0] = input[0] ^ aes_x_key[160];
	output[1] = input[1] ^ aes_x_key[161];
	output[2] = input[2] ^ aes_x_key[162];
	output[3] = input[3] ^ aes_x_key[163];
	output[4] = input[4] ^ aes_x_key[164];
	output[5] = input[5] ^ aes_x_key[165];
	output[6] = input[6] ^ aes_x_key[166];
	output[7] = input[7] ^ aes_x_key[167];
	output[8] = input[8] ^ aes_x_key[168];
	output[9] = input[9] ^ aes_x_key[169];
	output[10] = input[10] ^ aes_x_key[170];
	output[11] = input[11] ^ aes_x_key[171];
	output[12] = input[12] ^ aes_x_key[172];
	output[13] = input[13] ^ aes_x_key[173];
	output[14] = input[14] ^ aes_x_key[174];
	output[15] = input[15] ^ aes_x_key[175];

	// The 10th round (no MixColumns step in this round).
	// row 0
	output[0] = aes_inverse[output[0]] ^ aes_x_key[9 * 16];
	output[4] = aes_inverse[output[4]] ^ aes_x_key[9 * 16 + 4];
	output[8] = aes_inverse[output[8]] ^ aes_x_key[9 * 16 + 8];
	output[12] = aes_inverse[output[12]] ^ aes_x_key[9 * 16 + 12];
	// row 1
	buf1 = aes_inverse[output[13]] ^ aes_x_key[9 * 16 + 1];
	output[13] = aes_inverse[output[9]] ^ aes_x_key[9 * 16 + 13];
	output[9] = aes_inverse[output[5]] ^ aes_x_key[9 * 16 + 9];
	output[5] = aes_inverse[output[1]] ^ aes_x_key[9 * 16 + 5];
	output[1] = buf1;
	// row 2
	buf1 = aes_inverse[output[2]] ^ aes_x_key[9 * 16 + 10];
	buf2 = aes_inverse[output[6]] ^ aes_x_key[9 * 16 + 14];
	output[2] = aes_inverse[output[10]] ^ aes_x_key[9 * 16 + 2];
	output[6] = aes_inverse[output[14]] ^ aes_x_key[9 * 16 + 6];
	output[10] = buf1;
	output[14] = buf2;
	// row 3
	buf1 = aes_inverse[output[3]] ^ aes_x_key[9 * 16 + 15];
	output[3] = aes_inverse[output[7]] ^ aes_x_key[9 * 16 + 3];
	output[7] = aes_inverse[output[11]] ^ aes_x_key[9 * 16 + 7];
	output[11] = aes_inverse[output[15]] ^ aes_x_key[9 * 16 + 11];
	output[15] = buf1;

	// The 9th round to 1st round.
	for(round = 9; round;){
		--round;
		base = round << 4;
		// MixColumns
		// col1
		buf1 = aes_galois_mul2(aes_galois_mul2(output[0] ^ output[2]));
		buf2 = aes_galois_mul2(aes_galois_mul2(output[1] ^ output[3]));
		output[0] ^= buf1;
		output[1] ^= buf2;
		output[2] ^= buf1;
		output[3] ^= buf2;

		buf1 = output[0] ^ output[1] ^ output[2] ^ output[3];
		buf2 = output[0];
		buf3 = output[0] ^ output[1];
		buf3 = aes_galois_mul2(buf3);
		output[0] = output[0] ^ buf3 ^ buf1;
		buf3 = output[1] ^ output[2];
		buf3 = aes_galois_mul2(buf3);
		output[1] = output[1] ^ buf3 ^ buf1;
		buf3 = output[2] ^ output[3];
		buf3 = aes_galois_mul2(buf3);
		output[2] = output[2] ^ buf3 ^ buf1;
		buf3 = output[3] ^ buf2;
		buf3 = aes_galois_mul2(buf3);
		output[3] = output[3] ^ buf3 ^ buf1;

		// col2
		buf1 = aes_galois_mul2(aes_galois_mul2(output[4] ^ output[6]));
		buf2 = aes_galois_mul2(aes_galois_mul2(output[5] ^ output[7]));
		output[4] ^= buf1;
		output[5] ^= buf2;
		output[6] ^= buf1;
		output[7] ^= buf2;

		buf1 = output[4] ^ output[5] ^ output[6] ^ output[7];
		buf2 = output[4];
		buf3 = output[4] ^ output[5];
		buf3 = aes_galois_mul2(buf3);
		output[4] = output[4] ^ buf3 ^ buf1;
		buf3 = output[5] ^ output[6];
		buf3 = aes_galois_mul2(buf3);
		output[5] = output[5] ^ buf3 ^ buf1;
		buf3 = output[6] ^ output[7];
		buf3 = aes_galois_mul2(buf3);
		output[6] = output[6] ^ buf3 ^ buf1;
		buf3 = output[7] ^ buf2;
		buf3 = aes_galois_mul2(buf3);
		output[7] = output[7] ^ buf3 ^ buf1;

		// col3
		buf1 = aes_galois_mul2(aes_galois_mul2(output[8] ^ output[10]));
		buf2 = aes_galois_mul2(aes_galois_mul2(output[9] ^ output[11]));
		output[8] ^= buf1;
		output[9] ^= buf2;
		output[10] ^= buf1;
		output[11] ^= buf2;

		buf1 = output[8] ^ output[9] ^ output[10] ^ output[11];
		buf2 = output[8];
		buf3 = output[8] ^ output[9];
		buf3 = aes_galois_mul2(buf3);
		output[8] = output[8] ^ buf3 ^ buf1;
		buf3 = output[9] ^ output[10];
		buf3 = aes_galois_mul2(buf3);
		output[9] = output[9] ^ buf3 ^ buf1;
		buf3 = output[10] ^ output[11];
		buf3 = aes_galois_mul2(buf3);
		output[10] = output[10] ^ buf3 ^ buf1;
		buf3 = output[11] ^ buf2;
		buf3 = aes_galois_mul2(buf3);
		output[11] = output[11] ^ buf3 ^ buf1;

		// col4
		buf1 = aes_galois_mul2(aes_galois_mul2(output[12] ^ output[14]));
		buf2 = aes_galois_mul2(aes_galois_mul2(output[13] ^ output[15]));
		output[12] ^= buf1;
		output[13] ^= buf2;
		output[14] ^= buf1;
		output[15] ^= buf2;

		buf1 = output[12] ^ output[13] ^ output[14] ^ output[15];
		buf2 = output[12];
		buf3 = output[12] ^ output[13];
		buf3 = aes_galois_mul2(buf3);
		output[12] = output[12] ^ buf3 ^ buf1;
		buf3 = output[13] ^ output[14];
		buf3 = aes_galois_mul2(buf3);
		output[13] = output[13] ^ buf3 ^ buf1;
		buf3 = output[14] ^ output[15];
		buf3 = aes_galois_mul2(buf3);
		output[14] = output[14] ^ buf3 ^ buf1;
		buf3 = output[15] ^ buf2;
		buf3 = aes_galois_mul2(buf3);
		output[15] = output[15] ^ buf3 ^ buf1;

		// AddRoundKey, Reverse SubBytes and ShiftRows
		// row 0
		output[0] = aes_inverse[output[0]] ^ aes_x_key[base];
		output[4] = aes_inverse[output[4]] ^ aes_x_key[base + 4];
		output[8] = aes_inverse[output[8]] ^ aes_x_key[base + 8];
		output[12] = aes_inverse[output[12]] ^ aes_x_key[base + 12];
		// row 1
		buf1 = aes_inverse[output[13]] ^ aes_x_key[base + 1];
		output[13] = aes_inverse[output[9]] ^ aes_x_key[base + 13];
		output[9] = aes_inverse[output[5]] ^ aes_x_key[base + 9];
		output[5] = aes_inverse[output[1]] ^ aes_x_key[base + 5];
		output[1] = buf1;
		// row 2
		buf1 = aes_inverse[output[2]] ^ aes_x_key[base + 10];
		buf2 = aes_inverse[output[6]] ^ aes_x_key[base + 14];
		output[2] = aes_inverse[output[10]] ^ aes_x_key[base + 2];
		output[6] = aes_inverse[output[14]] ^ aes_x_key[base + 6];
		output[10] = buf1;
		output[14] = buf2;
		// row 3
		buf1 = aes_inverse[output[3]] ^ aes_x_key[base + 15];
		output[3] = aes_inverse[output[7]] ^ aes_x_key[base + 3];
		output[7] = aes_inverse[output[11]] ^ aes_x_key[base + 7];
		output[11] = aes_inverse[output[15]] ^ aes_x_key[base + 11];
		output[15] = buf1;
	}
}

