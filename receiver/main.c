/*
 * main.c
 */
#include <msp430.h> 
#include "nrf24l01.h"
#include <descriptors.h>
#include <USB_API/USB_Common/device.h>
#include <USB_API/USB_Common/types.h>
#include <USB_API/USB_Common/usb.h> //USB specific function
#include <core/HAL_UCS.h>
#include <core/HAL_PMM.h>

#include <USB_API/USB_HID_API/UsbHid.h>
#include "usb_hid_util.h"
#include "typedef.h"
#include "aes.h"
#include "usb_shared.h"
#include <string.h>
#include <stdlib.h>
#include <time.h>

/**
 * Initial all GPIO port.
 */
inline void port_init(void);

/**
 * Initial USB Clock.
 */
inline void clock_init(void);

/**
 * Update CRC for report.
 */
inline void report_random_postback(void);

/**
 * Check CRC of report is valid or not.
 *
 * @return	true if CRC is valid.
 * 			false otherwise.
 */
uint8_t crc_is_valid(void);

/**
 * Check CRC of CRC request is valid or not.
 *
 * @return	true if CRC is valid.
 * 			false otherwise.
 */
uint8_t crc_is_valid_req(void);

/**
 * Send CRC key from USB module to Keyboard module.
 */
inline void send_crc_key(void);

/**
 * USB HID Report.
 */
report_t report;

/**
 * Report which post-back to transmitter module.
 */
report_t postback = {{
		0xfa69dead, /* Header */
		0x00, 0x00, 0xFF, /* CRC, updated, empty_slots */
		0x00, 0x00, 0x00, 0x00, /* report data. */
}};

/**
 * Buffer for read/write to nRF24L01+.
 */
uint8_t nrf24l01_buffer[16];

/**
 * Key used for Initial CRC algorithm.
 */
uint16_t crc_seed_key;

int main(void) {
#if 0
#ifdef SYNC_CAP_NUM_LCK
	uint8_t usb_read_buf[64];
	memset(usb_read_buf, 0, 64);
#endif
#endif
	WDTCTL = WDTPW | WDTHOLD;	// Stop watch-dog timer
	port_init();
	SetVCore(3);
	clock_init();
	USB_init();
	USB_setEnabledEvents(
			kUSB_VbusOnEvent | kUSB_VbusOffEvent | kUSB_receiveCompletedEvent
					| kUSB_UsbSuspendEvent | kUSB_UsbResumeEvent
					| kUSB_dataReceivedEvent);
	if (USB_connectionInfo() & kUSB_vbusPresent) {
		USB_handleVbusOnEvent();
	}
	nrf24l01_init();

	_enable_interrupts();
	send_crc_key();

	while (1) {
		switch (USB_connectionState()) {
		case ST_USB_DISCONNECTED:
			_bis_SR_register(LPM3_bits + GIE);
			_nop();
			break;
		case ST_USB_CONNECTED_NO_ENUM:
			break;
		case ST_ENUM_ACTIVE:
			// Sleep until receive key from keyboard.
			_bis_SR_register(LPM0_bits + GIE);
			_disable_interrupts();
#ifdef SYNC_CAP_NUM_LCK
			if (usb_hid_is_data_received) {
				usb_hid_is_data_received = USBHID_receiveReport(
						postback.report.keys.keys, 0);
				if (usb_hid_is_data_received == kUSBHID_receiveCompleted) {
					postback.report.updated = 0x01;
					report_random_postback();
//					aes_encrypt(postback.data, nrf24l01_buffer);
//					nrf24l01_write(nrf24l01_buffer, 16);
				}
				usb_hid_is_data_received = 0;
				_nop();
			}
#endif
			while (nrf24l01_rx_buffer_not_empty()) {
				nrf24l01_read(nrf24l01_buffer, 16);
				aes_decrypt(nrf24l01_buffer, report.data);
				if (postback.report.updated) {
					aes_encrypt(postback.data, nrf24l01_buffer);
					nrf24l01_write(nrf24l01_buffer, 16);
					postback.report.updated = 0;
				}
				if (report.report.header == 0xfa69dead) {
					if (report.report.updated == 1) {
						if (crc_is_valid()) {
							USBHID_sendReport(report.report.keys.keys,
									HID0_INTFNUM);
							_nop();
						}
					} else if (report.report.updated == 0xFF) {
						if (crc_is_valid_req()) {
							send_crc_key();
						}
					}
				}
			}
			;
#ifdef SYNC_CAP_NUM_LCK

#endif
			_enable_interrupts();
			break;
		case ST_ENUM_SUSPENDED:
			_bis_SR_register(LPM3_bits + GIE);
			//Enter LPM3 w/interrupt.  Nothing for us to do while
			break;  //suspended.  (Remote wakeup isn't enabled in this example.)

		case ST_ENUM_IN_PROGRESS:
			break;
		case ST_NOENUM_SUSPENDED:
			_bis_SR_register(LPM3_bits + GIE);
			break;
		case ST_ERROR:
			_nop();
			break;
		default:
			break;
		}
	}
}

/**
 * Update CRC for report.
 */
inline void report_random_postback(void) {
	// Reset the CRC initialization and result register.
	CRCINIRES = crc_seed_key;
	srand(time(NULL));
	postback.report.keys.keys[1] = (uint8_t)(rand() & 0xFF);
	// Process input data.
	CRCDI = postback.report.keys.data[0];
	CRCDI = postback.report.keys.data[1] = rand();
	CRCDI = postback.report.keys.data[2] = rand();
	CRCDI = postback.report.keys.data[3] = rand();
	// Get Result.
	postback.report.crc = CRCINIRES;
}

/**
 * Check CRC of report is valid or not.
 *
 * @return	true if CRC is valid.
 * 			false otherwise.
 */
uint8_t crc_is_valid() {
	// Reset the CRC initialization and result register.
	CRCINIRES = crc_seed_key;
	// Process input data.
	CRCDI = report.report.keys.data[0];
	CRCDI = report.report.keys.data[1];
	CRCDI = report.report.keys.data[2];
	CRCDI = report.report.keys.data[3];
	// Get Result.
	return CRCINIRES == report.report.crc;
}

/**
 * Check CRC of CRC request is valid or not.
 *
 * @return	true if CRC is valid.
 * 			false otherwise.
 */
uint8_t crc_is_valid_req(void) {
	// Reset the CRC initialization and result register.
	CRCINIRES = 0x69FAu;
	// Process input data.
	CRCDI = report.report.keys.data[0];
	CRCDI = report.report.keys.data[1];
	CRCDI = report.report.keys.data[2];
	CRCDI = report.report.keys.data[3];
	// Get Result.
	return CRCINIRES == report.report.crc;
}

/**
 * Send CRC key from USB module to Keyboard module.
 */
inline void send_crc_key(void) {
	report.report.header = 0xfa69dead;

	srand(time(NULL));
	// Reset the CRC initialization and result register.
	CRCINIRES = 0x69FAu;
	// Generate seed key.
	CRCDI = crc_seed_key = report.report.keys.data[0] = rand();
	// Generate addition fake data.
	CRCDI = report.report.keys.data[1] = rand();
	CRCDI = report.report.keys.data[2] = rand();
	CRCDI = report.report.keys.data[3] = rand();
	// Update new CRC.
	report.report.crc = CRCINIRES;
	report.report.updated = 0xFF;

	aes_encrypt(report.data, nrf24l01_buffer);
	nrf24l01_write(nrf24l01_buffer, 16);
}

/**
 * Trap all unused interrupt.
 *
 * Trap all unused interrupt to prevent  jumping to unknown addresses,
 * execution of erroneous code which at the very least could lead to
 * application crashing, unexpected application behavior, or more
 * severely memory erasure or electrical damage.
 * TI compiler's Warning #10374.
 */
#pragma vector=ADC12_VECTOR
#pragma vector=COMP_B_VECTOR
#pragma vector=DAC12_VECTOR
#pragma vector=DMA_VECTOR
#pragma vector=LCD_B_VECTOR
#pragma vector=PORT1_VECTOR
#pragma vector=PORT3_VECTOR
#pragma vector=PORT4_VECTOR
#pragma vector=RTC_VECTOR
#pragma vector=SYSNMI_VECTOR
#pragma vector=TIMER0_A0_VECTOR
#pragma vector=TIMER0_A1_VECTOR
#pragma vector=TIMER0_B0_VECTOR
#pragma vector=TIMER0_B1_VECTOR
#pragma vector=TIMER1_A0_VECTOR
#pragma vector=TIMER1_A1_VECTOR
#pragma vector=TIMER2_A0_VECTOR
#pragma vector=TIMER2_A1_VECTOR
#pragma vector=USCI_A0_VECTOR
#pragma vector=USCI_A1_VECTOR
#pragma vector=USCI_A2_VECTOR
#pragma vector=USCI_B1_VECTOR
#pragma vector=USCI_B2_VECTOR
#pragma vector=WDT_VECTOR
__interrupt void unused_irq(void) {
	// Uncomment this line for debugging purpose
	// while (1);
	// Uncomment this line for reset purpose
	// PMMCTL0 = PMMPW | PMMSWBOR;         // Apply PMM password and trigger SW BOR
}

/**
 * Initial all GPIO port.
 */
void port_init(void) {
	PADIR = 0xFFFF;
	PAOUT = 0x00;
	PBDIR = 0xFF;
	PBOUT = 0x00;
	PCDIR = 0xFF;
	PCOUT = 0x00;
	PDDIR = 0xFFFF;
	PDOUT = 0x00;
	PEDIR = 0xFFFF;
	PEOUT = 0x00;
}

/**
 * Initial USB Clock.
 */
void clock_init(void) {
	if (USB_PLL_XT == 2) {
#if defined (__MSP430F552x) || defined (__MSP430F550x)
		P5SEL |= 0x0C;                               //enable XT2 pins for F5529
#elif defined (__MSP430F563x_F663x) || defined (__MSP430F565x_F665x)
		P7SEL |= 0x0C;
#endif

		//use REFO for FLL and ACLK
		UCSCTL3 = (UCSCTL3 & ~(SELREF_7)) | (SELREF__REFOCLK);
		UCSCTL4 = (UCSCTL4 & ~(SELA_7)) | (SELA__REFOCLK);

		//MCLK will be driven by the FLL (not by XT2), referenced to the REFO
		Init_FLL_Settle(USB_MCLK_FREQ / 1000, USB_MCLK_FREQ / 32768); //Start the FLL, at the freq indicated by the config
																	  //constant USB_MCLK_FREQ
		XT2_Start(XT2DRIVE_0);                         //Start the "USB crystal"
	} else {
#if defined (__MSP430F552x) || defined (__MSP430F550x)
		P5SEL |= 0x10;                                      //enable XT1 pins
#endif
		//Use the REFO oscillator to source the FLL and ACLK
		UCSCTL3 = SELREF__REFOCLK;
		UCSCTL4 = (UCSCTL4 & ~(SELA_7)) | (SELA__REFOCLK);

		//MCLK will be driven by the FLL (not by XT2), referenced to the REFO
		Init_FLL_Settle(USB_MCLK_FREQ / 1000, USB_MCLK_FREQ / 32768); //set FLL (DCOCLK)

		XT1_Start(XT1DRIVE_0);                         //Start the "USB crystal"
	}
}

/*
 * ======== UNMI_ISR ========
 */
#pragma vector = UNMI_VECTOR
__interrupt void UNMI_ISR(void) {
	switch (__even_in_range(SYSUNIV, SYSUNIV_BUSIFG)) {
	case SYSUNIV_NONE:
		__no_operation();
		break;
	case SYSUNIV_NMIIFG:
		__no_operation();
		break;
	case SYSUNIV_OFIFG:
		UCSCTL7 &= ~(DCOFFG + XT1LFOFFG + XT2OFFG); //Clear OSC flaut Flags fault flags
		SFRIFG1 &= ~OFIFG;                          //Clear OFIFG fault flag
		break;
	case SYSUNIV_ACCVIFG:
		__no_operation();
		break;
	case SYSUNIV_BUSIFG:
		//If bus error occured - the cleaning of flag and re-initializing of USB is
		//required.
		SYSBERRIV = 0;                          //clear bus error flag
		// USB_disable();                          //Disable
		USBKEYPID = 0x9628;             //set KEY and PID to 0x9628 -> access to
										//configuration registers enabled
		USBCNF = 0;                                         //disable USB module
		USBPLLCTL &= ~UPLLEN;                                      //disable PLL
		USBKEYPID = 0x9600;         //access to configuration registers disabled
		bEnumerationStatus = 0x00;                    //device is not enumerated
		bFunctionSuspended = FALSE;                    //device is not suspended
	}
}
