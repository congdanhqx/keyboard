/*
 * nrf24l01.c
 *
 *  Created on: Jul 2, 2013
 *      Author: Cong Danh
 */
#include "nrf24l01.h"

#include <msp430.h>
#include "spi.h"
#include "nrf24l01_register.h"

#include "config.h"

#define RX_PW 16

// Private function declaration.

#define nrf24l01_write_register(reg,data,len)	nrf24l01_execute_command(W_REGISTER(reg), data, len, 0)
#define nrf24l01_read_register(reg,data,len)	nrf24l01_execute_command(R_REGISTER(reg), data, len, 1)
#define nrf24l01_flush_tx()						nrf24l01_execute_command(FLUSH_TX, NULL, 0, 1)
#define nrf24l01_flush_rx()						nrf24l01_execute_command(FLUSH_RX, NULL, 0, 1)
#define nrf24l01_reuse_tx_pl()					nrf24l01_execute_command(REUSE_TX_PL, NULL, 0, 1)
#define nrf24l01_nop()							nrf24l01_execute_command(NRF24L01_NOP, NULL, 0, 1)

/* START: Initial Function */

/**
 * Set nRF24L01+ RX address.
 *
 * This function should be marked inline to increase performance.
 *
 * @param addr RX address, must be 5 byte length.
 */
inline void    nrf24l01_set_rx_address(uint8_t *addr);
/**
 * Set nRF24L01+ TX address.
 *
 * This function also set RX pipe 0 address to addr
 * This function should be marked inline to increase performance.
 *
 * @param addr TX address, must be 5 byte length.
 */
inline void    nrf24l01_set_tx_address(uint8_t *addr);

/* END: Initial Function */

/* START: Get State Function */
/**
 * Get the ready state of RX of nRF24L01+.
 *
 * @return	true if data in the RX FIFO,
 * 			false otherwise.
 */
uint8_t	nrf24l01_rx_ready(void);
/**
 * Get nRF24L01+ FIFO's status.
 *
 * @return FIFO_STATUS register value as described in page 61
 * 			of nRF24L01+ specification.
 */
uint8_t	nrf24l01_get_fifo_status(void);
/**
 * Get last sent message's status.
 *
 * @return	0 if sent successful.
 * 			-1 if sent fail.
 * 			1 if sending is in progress.
 */
int8_t	nrf24l01_get_last_message_status(void);
/* END: Get State Function */

/**
 * Change nRF24L01+ to RX Mode.
 */
inline void	nrf24l01_to_rx(void);
/**
 * Change nRF24L01+ to TX Mode.
 */
inline void	nrf24l01_to_tx(void);
/**
 * Change nRF24L01+ to Power Down state.
 */
inline void	nrf24l01_power_down(void);
/**
 * Get nRF24L01+ CONFIG register's value.
 *
 * @return nRF24L01+ CONFIG register's value.
 */
uint8_t	nrf24l01_get_config(void);
/**
 * Set High Output to RF_CSN pin.
 */
inline void nrf24l01_set_csn(void);
/**
 * Set Low Output to RF_CSN pin.
 */
inline void nrf24l01_clear_csn(void);
/**
 * Set High Output to RF_CE pin.
 */
inline void nrf24l01_set_ce(void);
/**
 * Set Low Output to RF_CE pin.
 */
inline void nrf24l01_clear_ce(void);
/**
 * Configure value of a register of nRF24L01+.
 *
 * @param reg	nRF24L01+ register.
 * @param value	value to set.
 * @return value of STATUS register.
 */
uint8_t nrf24l01_config_register(uint8_t reg, uint8_t value);
/**
 * Low level api for communicate with nRF24L01+.
 *
 * @param instruction	instruction
 * @param data			data
 * @param len			length of data
 * @param copydata		1 if you want read back,
 */
uint8_t nrf24l01_execute_command(uint8_t instruction, uint8_t* data,
		unsigned int len, uint8_t copydata);
// End Private function declaration.

/**
 * Indicate the MCU is woke up by nRF24L01+ RX or not.
 *
 * @value	1	if MCU is woke up by nRF24L01+ RX.
 * 			0 otherwise.
 */
uint8_t nrf24l01_is_usb_post_back;

/**
 * Initialize nRF24L01+ module.
 */
void nrf24l01_init() {

	spi_init();
	nrf24l01_is_usb_post_back = 0;
	RF_DIR |= (RF_CSN | RF_CE);
	IRQ_OUT |= NORDIC_IRQ;
	IRQ_DIR &= (uint8_t)~NORDIC_IRQ;
	IRQ_REN |= NORDIC_IRQ;
	IRQ_SEL &= (uint8_t)~NORDIC_IRQ;
	IRQ_IE = (uint8_t)NORDIC_IRQ;
	IRQ_IES |= NORDIC_IRQ;

	// RF Channel to default
	nrf24l01_config_register(ADDR_RF_CH, RF_CH_DEFAULT);

	// Initial payload width
	nrf24l01_config_register(RX_PW_P0, RX_PW_DEFAULT);
	nrf24l01_config_register(RX_PW_P1, RX_PW);
	nrf24l01_config_register(RX_PW_P2, RX_PW_DEFAULT);
	nrf24l01_config_register(RX_PW_P3, RX_PW_DEFAULT);
	nrf24l01_config_register(RX_PW_P4, RX_PW_DEFAULT);
	nrf24l01_config_register(RX_PW_P5, RX_PW_DEFAULT);

	// Setup RF
	nrf24l01_config_register(ADDR_RF_SETUP, RF_PWR_6 | RF_DR_1M);

	// Default configuration.
	nrf24l01_config_register(ADDR_CONFIG, CONFIG_DEFAULT);

	// Auto Ack: Default.
	nrf24l01_config_register(ADDR_EN_AA, EN_AA_DEFAULT);

	// Enable RX Address: Pipe 0 and Pipe 1, default.
	nrf24l01_config_register(ADDR_EN_RXADDR, ERX_DEFAULT);

	// Setup retransmit: Delay 4ms, up to 3 times retransmit.
	nrf24l01_config_register(ADDR_SETUP_RETR, ARD_4000 | ARC_3);

	// Set Address Width default (5 bytes --> use MSB).
	nrf24l01_config_register(ADDR_SETUP_AW, SETUP_AW_DEFAULT);

	// Use static payload length.
	nrf24l01_config_register(DYNPD, DYNPD_DEFAULT);

	uint8_t rx_addr[5] = RF_RX_ADDR;
	nrf24l01_set_rx_address(rx_addr);
	uint8_t tx_addr[5] = RF_TX_ADDR;
	nrf24l01_set_tx_address(tx_addr);

	nrf24l01_clear_csn();
	spi_xferchar(FLUSH_RX);
	nrf24l01_set_csn();

	nrf24l01_config_register(ADDR_STATUS, STATUS_ALL_IR);

	nrf24l01_clear_ce();
	nrf24l01_config_register(ADDR_CONFIG, CONFIG_PWR_RX);
	nrf24l01_set_ce();

	__delay_cycles(_2MS);

	// Clear Interrupt flags.
	IRQ_IFG = 0;
}

/**
 * PORT2/nRF24L01+ Interrupt request handler.
 *
 * Process interrupt request sent from nRF24L01+ module.
 * Interrupt occurred when the IRQ_PIN come from 1 to 0.
 */
#pragma vector=PORT2_VECTOR
__interrupt void port2_irq() {
	if (IRQ_IFG & NORDIC_IRQ) {
		_delay_cycles(0x1FF);
		if (!(IRQ_IN & NORDIC_IRQ)) {
			nrf24l01_clear_csn();
			uint8_t msg_status = spi_xferchar(NRF24L01_NOP);
			nrf24l01_set_csn();

			if(msg_status & TX_DS){
				// TODO: Put code to work when Transmit successful.
				nrf24l01_to_rx();
				_nop();
			} else if(msg_status & RX_DR) {
				nrf24l01_is_usb_post_back = 1;
				LPM3_EXIT;
				_nop();
			} else if (msg_status & MAX_RT) {
				// TODO: Put code here to work when Max Retransmission reached.
				nrf24l01_to_rx();
				_nop();
			} else {
			}
		}
		nrf24l01_config_register(ADDR_STATUS, STATUS_ALL_IR);
		IRQ_IFG = 0;
	}
}

/**
 * Set nRF24L01+ RX address.
 *
 * This function should be marked inline to increase performance.
 *
 * @param addr RX address, must be 5 byte length.
 */
void nrf24l01_set_rx_address(uint8_t *addr) {
	nrf24l01_clear_ce();
	nrf24l01_write_register(RX_ADDR_P1, addr, 5);
	nrf24l01_set_ce();
}

/**
 * Set nRF24L01+ TX address.
 *
 * This function also set RX pipe 0 address to addr
 * This function should be marked inline to increase performance.
 *
 * @param addr TX address, must be 5 byte length.
 */
void nrf24l01_set_tx_address(uint8_t *addr) {
	nrf24l01_write_register(RX_ADDR_P0, addr, 5);
	nrf24l01_write_register(TX_ADDR, addr, 5);
}

/* START: Get State Function */
/**
 * Get the ready state of RX of nRF24L01+.
 *
 * @return	true if data in the RX FIFO,
 * 			false otherwise.
 */
uint8_t nrf24l01_rx_ready(void) {
	nrf24l01_clear_csn();
	uint8_t rx_status = spi_xferchar(NRF24L01_NOP);
	nrf24l01_set_csn();
	return (rx_status & RX_DR);
	/*	return 1;

	 nrf24l01_read_register(FIFO_STATUS, &rx_status, 1);
	 return !(rx_status & RX_EMPTY);*/
}

/**
 * Get nRF24L01+ FIFO's status.
 *
 * @return FIFO_STATUS register value as described in page 61
 * 			of nRF24L01+ specification.
 */
uint8_t nrf24l01_get_fifo_status(void) {
	uint8_t fifo_status;
	nrf24l01_read_register(FIFO_STATUS, &fifo_status, 1);
	return fifo_status;
}

/**
 * Get last sent message's status.
 *
 * @return	0 if sent successful.
 * 			-1 if sent fail.
 * 			1 if sending is in progress.
 */
int8_t nrf24l01_get_last_message_status(void) {
	nrf24l01_clear_csn();
	uint8_t msg_status = spi_xferchar(NRF24L01_NOP);
	nrf24l01_set_csn();

	if (msg_status & TX_DS) {
		return 0;
	}
	if (msg_status & MAX_RT) {
		return -1;
	}
	return 1;
}

/**
 * Send data via nRF24L01+.
 *
 * @param data data to send.
 * @param length length of sent data.
 * @return value of STATUS register of nRF24L01+.
 */
uint8_t nrf24l01_write(uint8_t* data, int len) {
	uint8_t status_write;
	unsigned int i;
	// Switch to Standby I Mode.
	nrf24l01_clear_ce();
	nrf24l01_to_tx();
	/* Do we really need to flush TX FIFO each time ? */
#ifdef ALWAYS_FLUSH_TX
	/* Pull down chip select */
	nrf24l01_clear_csn();
	/* Write command to flush transmit FIFO */
	spi_xferchar(FLUSH_TX);
	/* Pull up chip select */
	nrf24l01_set_csn();
#endif
	nrf24l01_clear_csn();
	status_write = spi_xferchar(W_TX_PAYLOAD);
	for (i = 0; i < len; ++i) {
		spi_xferchar(data[i]);
	}
	nrf24l01_set_csn();
	// Start transmission.
	nrf24l01_set_ce();

	return status_write;
}

/**
 * Read data arrived at nRF24L01+.
 *
 * @param buffer read buffer.
 * @param len	 number of byte to read.
 * @return	value of STATUS register of nRF24L01+.
 */
uint8_t nrf24l01_read(uint8_t* buffer, int len) {
	uint8_t read_status;
	unsigned int i;

	nrf24l01_clear_csn();
	read_status = spi_xferchar(R_RX_PAYLOAD);
	for (i = 0; i < len; ++i) {
		buffer[i] = spi_xferchar(NULL);
	}
	nrf24l01_set_csn();

	nrf24l01_config_register(ADDR_STATUS, RX_DR);

	return read_status;
}

/**
 * Check if RX buffer is empty or not.
 *
 * @return	1 if RX buffer is empty.
 * 			0 if RX buffer is not empty.
 */
uint8_t nrf24l01_rx_buffer_not_empty(void) {
	nrf24l01_clear_csn();
	uint8_t rx_status = spi_xferchar(NRF24L01_NOP);
	nrf24l01_set_csn();
	return (rx_status & RX_FIFO_EMPTY) != RX_FIFO_EMPTY;
}

/**
 * Change nRF24L01+ to RX Mode.
 */
inline void nrf24l01_to_rx(void) {
	nrf24l01_clear_csn();
	spi_xferchar(FLUSH_RX);
	nrf24l01_set_csn();

	nrf24l01_config_register(ADDR_STATUS, STATUS_ALL_IR);

	nrf24l01_clear_ce();
	nrf24l01_config_register(ADDR_CONFIG, CONFIG_PWR_RX);
	nrf24l01_set_ce();
}

/**
 * Change nRF24L01+ to TX Mode.
 */
inline void nrf24l01_to_tx(void) {
	nrf24l01_config_register(ADDR_STATUS, STATUS_ALL_IR);
	nrf24l01_config_register(ADDR_CONFIG, CONFIG_PWR_TX);
}

/**
 * Change nRF24L01+ to Power Down state.
 */
inline void nrf24l01_power_down(void) {
	nrf24l01_clear_ce();
	nrf24l01_config_register(ADDR_CONFIG, CONFIG_DEFAULT);
}

/**
 * Set High Output to RF_CSN pin.
 */
inline void nrf24l01_set_csn(void) {
	RF_OUT |= RF_CSN;
}

/**
 * Set Low Output to RF_CSN pin.
 */
inline void nrf24l01_clear_csn(void) {
	RF_OUT &= ~RF_CSN;
}

/**
 * Set High Output to RF_CE pin.
 */
inline void nrf24l01_set_ce(void) {
	RF_OUT |= RF_CE;
}

/**
 * Set Low Output to RF_CE pin.
 */
inline void nrf24l01_clear_ce(void) {
	RF_OUT &= ~RF_CE;
}

/**
 * Configure value of a register of nRF24L01+.
 *
 * @param reg	nRF24L01+ register.
 * @param value	value to set.
 * @return value of STATUS register.
 */
uint8_t nrf24l01_config_register(uint8_t reg, uint8_t value) {
	nrf24l01_clear_csn();
	uint8_t config_status = spi_xferchar(W_REGISTER(reg));
	spi_xferchar(value);
	nrf24l01_set_csn();
	// __delay_cycles(_5MS);
	return config_status;
}

/**
 * Get nRF24L01+ CONFIG register's value.
 *
 * @return nRF24L01+ CONFIG register's value.
 */
uint8_t nrf24l01_get_config(void) {
	nrf24l01_clear_csn();
	/*uint8_t config_status = */spi_xferchar(R_REGISTER(ADDR_CONFIG));
	uint8_t config_status = spi_xferchar(0xFF);
	nrf24l01_set_csn();
	return config_status;
}

/**
 * Low level api for communicate with nRF24L01+.
 *
 * @param instruction	instruction
 * @param data			data
 * @param len			length of data
 * @param copydata		1 if you want read back,
 */
uint8_t nrf24l01_execute_command(uint8_t instruction, uint8_t* data,
		unsigned int len, uint8_t copydata) {
	uint8_t cmd_status;
	unsigned int i;

	nrf24l01_clear_csn();

	cmd_status = spi_xferchar(instruction);
	if (copydata) {
		for (i = 0; i < len; ++i) {
			data[i] = spi_xferchar(data[i]);
		}
	} else {
		for (i = 0; i < len; ++i) {
			spi_xferchar(data[i]);
		}
	}

	nrf24l01_set_csn();

	__delay_cycles(_130US);
	return cmd_status;
}
